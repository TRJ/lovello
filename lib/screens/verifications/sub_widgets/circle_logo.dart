import 'package:flutter/material.dart';

class CircleLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    return  Container(
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(
              blurRadius: 10, color: Colors.grey, spreadRadius: 1)
        ],
      ),
      child: CircleAvatar(
        radius: height*.1,
        backgroundImage: AssetImage("images/lowello_logo.png"),
      ),
    );
  }
}

class CircleLogoWithRadius extends StatelessWidget {
  final double h;
  CircleLogoWithRadius({this.h});
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    return  Container(
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(
              blurRadius: 5, color: Colors.grey, spreadRadius: 1)
        ],
      ),
      child: CircleAvatar(
        radius: height*h,
        backgroundImage: AssetImage("images/lowello_logo.png"),
      ),
    );
  }
}
