import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/models/signup_data_model.dart';
import 'package:hive/screens/shared/constants.dart';
import 'package:hive/screens/shared/loading.dart';
import 'package:hive/services/verification_services.dart';

class LoginController {
  final BuildContext context;

  LoginController(this.context);
  VerificationServices ver = VerificationServices();

  doEmailLogin({String email, String password}) async {
    floatingLoading(context);
    int login = await ver.doLogin("email", email, password);
    Navigator.of(context).pop();
    if (login == 1) {
      Fluttertoast.showToast(
        msg: 'Login Successfull',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.grey,
        textColor: Colors.red,
        fontSize: 11.0,
      );
      Navigator.of(context).pushNamedAndRemoveUntil(
          "/landing", (Route<dynamic> route) => false,
          );

    } else if (login == 0) {
      CoolAlert.show(
          context: context,
          type: CoolAlertType.error,
          text: "Please enter a valid Email and Password");
    } else {
      CoolAlert.show(
          context: context, type: CoolAlertType.error, text: errorText);
    }
  }

  doEmailSignup(String email, String name, String password) async {
    floatingLoading(context);
    int otp = await ver.doSignUp(type: 'email', email: email);
    Navigator.of(context).pop();
    if (otp == 1) {
      Fluttertoast.showToast(
        msg: 'OTP sent to your email',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.grey,
        textColor: Colors.red,
        fontSize: 11.0,
      );
      SignupDataModel data =
          SignupDataModel(name: name, email: email, password: password);
      Navigator.of(context).pushNamedAndRemoveUntil(
          "/signup_otp", (Route<dynamic> route) => false,
          arguments: data);
    } else if (otp == 2) {
      CoolAlert.show(
          context: context,
          type: CoolAlertType.info,
          text: "Email Already exists");
    } else {
      CoolAlert.show(
          context: context, type: CoolAlertType.error, text: errorText);
    }
  }

   verifySignup(SignupDataModel data) async {
     floatingLoading(context);
     var signup =   await ver.verifySignUp(data);
     Navigator.of(context).pop();
     if(signup==null){
       CoolAlert.show(
           context: context, type: CoolAlertType.error, text: errorText);
     }else if(signup[0]==1){
       Fluttertoast.showToast(
         msg: 'Registration Successfull',
         toastLength: Toast.LENGTH_SHORT,
         gravity: ToastGravity.BOTTOM,
         timeInSecForIosWeb: 1,
         backgroundColor: Colors.grey,
         textColor: Colors.red,
         fontSize: 11.0,
       );
       Navigator.of(context).pushNamedAndRemoveUntil(
         "/landing", (Route<dynamic> route) => false,
       );

     }else if(signup[0]==2){
       CoolAlert.show(
           context: context,
           type: CoolAlertType.error,
           text:signup[1]??errorText);
     }else{
       CoolAlert.show(
           context: context, type: CoolAlertType.error, text: errorText);
     }
  }

  handleSignupFormError(bool email, bool password) {
    if (email) {
      Fluttertoast.showToast(
        msg: "Email Invalid",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.grey,
        textColor: Colors.red,
        fontSize: 11.0,
      );
    } else if (password) {
      Fluttertoast.showToast(
        msg: "Password & Confirm password does not match",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.grey,
        textColor: Colors.red,
        fontSize: 11.0,
      );
    } else {
      Fluttertoast.showToast(
        msg: "Please enter all the fields",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.grey,
        textColor: Colors.red,
        fontSize: 11.0,
      );
    }
  }
}
