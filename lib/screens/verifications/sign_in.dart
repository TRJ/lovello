
import 'package:flutter/material.dart';
import 'package:hive/screens/shared/constants.dart';
import 'package:hive/screens/shared/loading.dart';
import 'package:hive/screens/verifications/controllers/login_controller.dart';
import 'package:hive/screens/verifications/sub_widgets/circle_logo.dart';
import 'package:hive/screens/verifications/sub_widgets/signup_singin_rich_text.dart';


class NewSignInDesign extends StatefulWidget {
  @override
  _NewSignInDesignState createState() => _NewSignInDesignState();
}

class _NewSignInDesignState extends State<NewSignInDesign> {

  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool showPassword = false;
  onSingInPressed() async {
    if (_formKey.currentState.validate()) {
        LoginController(context).doEmailLogin(email: _email.text,password: _password.text);
    }
  }
  @override
  void dispose() {
    _email.dispose();
    _password.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
        child: ListView(
          children: [
            Container(
              height: height-20,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Color(0xFFF32408),Color(0xFFA82104)],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter
                  )
              ),

              child: Stack(
                children: [
                  Positioned(
                    top: height*0.08,
                    bottom: height*0.1,
                    left: 0,
                    right: 10,
                    child: Container(

                      child: Image(
                        fit: BoxFit.fill,
                        image: AssetImage("images/white_box.png"),

                      ),
                    ),
                  ),
                  Positioned(
                    top: height*0.035,
                    right: width*.07,
                    child: CircleLogo(),
                  ),
                  Positioned(
                    top: height*0.26,
                    left: 20,
                    child: Container(

                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          FrontTextInLogin(),
                          SizedBox(height: height*0.05,),
                          Container(

                            child: Form(
                              key: _formKey,
                              child: Container(

                                child: Column(
                                  children: [
                                    Container(
                                      width:width*0.6,
                                      padding:
                                      EdgeInsets.symmetric(vertical: 10,),
                                      child: TextFormField(

                                          validator: (val) {
                                            if (val.isEmpty) {
                                              return "Email is Required";
                                            } else if (!RegExp(
                                                r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                                .hasMatch(val)) {
                                              return "Email is not valid";
                                            }

                                            return null;
                                          },
                                          controller: _email,
                                          decoration: loginInputDecoration.copyWith(

                                              isDense: true,
                                              hintText: 'Email',
                                              prefixIcon: Icon(
                                                Icons.email_rounded,
                                                color: Color(0xFFBD2823),
                                              ))),
                                    ),
                                    Container(
                                      width:width*0.6,

                                      child: TextFormField(
                                        validator: (val) {
                                          if (val.isEmpty) {
                                            return "Password is Required";
                                          }

                                          return null;
                                        },
                                        controller: _password,
                                        obscureText: !showPassword,
                                        decoration: loginInputDecoration.copyWith(
                                          isDense: true,
                                          hintText: 'Password',
                                          suffixIcon: IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  showPassword = !showPassword;
                                                });
                                              },
                                              icon: Icon(
                                                showPassword
                                                    ? Icons.visibility_off
                                                    : Icons.visibility,
                                                color: Colors.black,
                                              )),
                                          prefixIcon: Icon(
                                            Icons.lock,
                                            color: Color(0xFFBD2823),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height:height *0.025,),
                        //  LinearSocialButtonGroup(),
                        ],
                      ),
                    ),

                  ),

                  Positioned(
                    top: height*0.65,
                    left: width*0.50,
                    child: ElevatedButton(
                      onPressed: onSingInPressed,
                      style: ButtonStyle(
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30),
                              )
                          ),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.black
                          )
                      ),
                      child: Text("SIGN IN",style: TextStyle(color: Colors.white),),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      child: SignInSignUpRichText(
                        plainText: "Don't have an account?",
                        coloredText: "Sign Up now !",
                        route: "/sign_up",
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}


class FrontTextInLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Let's Sign In",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          Text(
            "Welcome Back, you've been missed",
            style: TextStyle(color: Colors.grey[600], fontSize: 14),
          )
        ],
      ),
    );
  }
}
