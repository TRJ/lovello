import 'package:flutter/material.dart';

import 'package:hive/screens/nearby/interested_people.dart';
import 'package:hive/screens/nearby/selection_tabs.dart';
import 'package:hive/screens/withAuth/go_live/current_live.dart';

class NearbyPeopleTab extends StatefulWidget {
  @override
  _NearbyPeopleTabState createState() => _NearbyPeopleTabState();
}

class _NearbyPeopleTabState extends State<NearbyPeopleTab> {
  int selectedSection = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: [
          SizedBox(height: 10,),
            SelectionTabNearby(
              onTap: (val){
                setState(() {
                  selectedSection = val;
                });
              },
            ),
          SizedBox(height: 10,),
          InterestedPeople(),

          selectedSection==1?CurrentLive():selectedSection==3?Container():Container(
            child: Center(
              child: Image(
                image: AssetImage("images/coming_soon.png"),
              ),
            ),
          )
        ],
      ),
    );
  }
}
