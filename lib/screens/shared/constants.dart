import 'package:flutter/material.dart';

TextStyle lableStyle = TextStyle(fontWeight: FontWeight.w700,color: Colors.black);
TextStyle selectedLableStyle = TextStyle(fontWeight: FontWeight.w700,color:Color(0xFFAA2104) );

InputDecoration loginInputDecoration = InputDecoration(

    border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
        borderSide: BorderSide(color: Colors.grey[200])),
    focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
        borderSide: BorderSide(color: Colors.red)),
    hintStyle: TextStyle(color: Colors.grey));


InputDecoration signupInputDecoration = InputDecoration(
    contentPadding: EdgeInsets.all(8),
    errorStyle: TextStyle(height: 0,color: Colors.transparent),
    border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
        borderSide: BorderSide(color: Colors.grey[200])),
    focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
        borderSide: BorderSide(color: Colors.red)),
    hintStyle: TextStyle(color: Colors.grey));



Color primaryColor = Color(0xFFF32408);

String errorText = "Bad Internet Connection";
