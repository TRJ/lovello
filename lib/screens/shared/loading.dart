
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
 class Loading extends StatelessWidget {
   @override
   Widget build(BuildContext context) {
     return Container(
       color: Colors.transparent,
       child: Center(
         child: SpinKitPumpingHeart(
           color: Colors.red,

         ),
         // child: Image(
         // //  iconColor: Color(0xFFEFAA1F),
         //   width: MediaQuery.of(context).size.width,
         //   fit: BoxFit.fitWidth,
         //   image: AssetImage("images/loder.gif"),
         // ),
       ),
     );
   }
 }

floatingLoading(context){
  return showDialog(context: context,builder: (context){
    return Dialog(
      backgroundColor: Colors.transparent,
      child: Loading(),
    );
  });
}