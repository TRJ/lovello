
String baseURL = "https://api.lovello.tv/";
String assetURL = "https://admin.lovello.tv:8080/";
String countryAssetUrl = assetURL+"uploads/country_file/";
String profilePicUrl = baseURL+"uploads/user_file/";
String loginUrl = baseURL+"user-login";
String signupOtp = baseURL+"signup-otp";
String countryList = baseURL+"countries";
String verifySignup = baseURL+"verify-signup";
