import 'package:flutter/material.dart';
import 'package:hive/models/live_model.dart';
import 'package:hive/screens/live/pages/audio_live.dart';
import 'package:hive/screens/live/pages/call.dart';
import 'package:hive/screens/live/pages/multi_guest_live.dart';
import 'package:hive/services/firestore_services.dart';


class CurrentLive extends StatefulWidget {
  final String type;
  CurrentLive({this.type});
  @override
  _CurrentLiveState createState() => _CurrentLiveState();
}

class _CurrentLiveState extends State<CurrentLive> {
  List<String> imageList = [
    "img-01.jpg",
    "img-02.jpg",
    "img-03.jpg",
    "img-04.jpg",
    "img-05.jpg",
    "img-06.jpg",
    "img-07.jpg",
    "img-08.jpg"
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: StreamBuilder(
        stream: FirestoreServices().getLiveStreams(widget.type??"video"),
        builder: (context, snapshot) {
          // if(!snapshot.hasData){
          //   return Loading();
          // }
          if (!snapshot.hasData) {
            return Container();
          }
          if (snapshot.data.length == 0) {
            return Center(
              child: Text(
                "NO Live Stream Available",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            );
          }

          return GridView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 5.0,
              mainAxisSpacing: 5.0,
            ),
            itemCount: snapshot.data.length,
            itemBuilder: (context, i) {
              LiveModel live = snapshot.data[i];
              return GestureDetector(
                onTap: () async {
                  if (live.liveType == "video") {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CallPage(
                          channelName: live.liveId,
                          broadcastUser: live,
                          role: 2,
                        ),
                      ),
                    );
                  }else if(live.liveType=="multi_video"){
                    await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => MultiGuestLive(
                            channelName: live.liveId,
                            broadcastUser: live,
                            seats: live.seats,
                          ),
                        ));
                  }

                  else {
                    await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AudioLive(
                            channelName: live.liveId,
                            broadcastUser: live,

                          ),
                        ));
                  }
                },
                child: ImageBoxLive(
                  title: live.liveName,
                  image: live.photoUrl,
                ),
              );
            },
          );
        },
      ),
      //     child: GridView.builder(
      //   physics: NeverScrollableScrollPhysics(),
      //   shrinkWrap: true,
      //   gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
      //     crossAxisCount: 2,
      //     crossAxisSpacing: 5.0,
      //     mainAxisSpacing: 5.0,
      //   ),
      //   itemCount: 3,
      //   itemBuilder: (context, i) {
      //     return ImageBoxLive(
      //       title: "WHO REALLY FWM",
      //       image: "img-04.jpg",
      //     );
      //   },
      // )
    );
  }
}

class ImageBoxLive extends StatelessWidget {
  final String title;
  final String image;
  ImageBoxLive({this.title, this.image});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5),
      child: Container(
          height: MediaQuery.of(context).size.width / 2.2,
          width: MediaQuery.of(context).size.width / 2.2,
          decoration: BoxDecoration(
            // border: Border.all(
            //   color: Color(0xFFEFAA1F),
            // ),

            image: DecorationImage(
                fit: BoxFit.cover,
                image: image != null
                    ? NetworkImage(image)
                    : AssetImage("images/user-large.png")),
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            color: Colors.redAccent,
          ),
          child: Container(
            child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    gradient: LinearGradient(
                        colors: [Colors.transparent, Colors.red],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: [.7, 1]),
                  ),
                ),
                Positioned(
                  bottom: 0,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      title ?? "  ",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w500),
                    ),
                  ),
                )
              ],
            ),
          )),
    );
  }
}
