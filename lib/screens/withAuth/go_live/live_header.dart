import 'package:flutter/material.dart';
import 'package:hive/models/current_user_model.dart';
import 'package:hive/models/live_model.dart';
import 'package:hive/models/viewer_model.dart';
import 'package:hive/screens/shared/initIcon_container.dart';
import 'package:hive/screens/shared/network_image.dart';
import 'package:hive/services/firestore_services.dart';
import 'package:hive/services/token_manager_services.dart';

class LiveHeader extends StatefulWidget {
  final LiveModel broadcastUser;
  final bool broadcaster;
  final String channelName;
  final Function onClose;
  final Function onView;
  LiveHeader({this.broadcastUser, this.broadcaster, this.channelName,this.onClose,this.onView});
  @override
  _LiveHeaderState createState() => _LiveHeaderState();
}

class _LiveHeaderState extends State<LiveHeader> {
  String photoUrl;
  String name = "";

  @override
  void initState() {
    getInitialDatas();
    super.initState();
  }

  getInitialDatas() async {
    if (widget.broadcaster) {
      CurrentUserModel user = await TokenManagerServices().getData();
      photoUrl = user.image;
      name = user.name;
      print(name);
    } else {
      photoUrl = widget.broadcastUser.photoUrl;
      name = widget.broadcastUser.liveName;
    }
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 10,
      top: 10,
      right: 10,
      //   right: MediaQuery.of(context).size.width / 2.5,
      child: Container(
        height: 54,
        width: MediaQuery.of(context).size.width,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Container(
                height: 40,
                decoration: BoxDecoration(
                    color: Color(0x99000000),
                    borderRadius: BorderRadius.circular(40)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    photoUrl != null
                        ? CircleAvatar(
                            radius: 20,
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(40),
                                child: HexagonProfilePicNetworkImage(
                                  url: photoUrl,
                                )),
                          )
                        : InitIconContainer(
                            radius: 40,
                            text: name,
                          ),
                    Expanded(
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        // width: MediaQuery.of(context).size.width*0.4,
                        child: Text(
                          name,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 13,
                              fontWeight: FontWeight.w600),
                        ),
                      ),

                    )
                  ],
                ),
              ),
            ),
            Expanded(
              child: Container(
                child: StreamBuilder(
                  stream:
                      FirestoreServices().getViewersList(widget.channelName),
                  builder: (context, snapshot) {
                    List<Widget> people = [];
                    if (snapshot.hasData) {
                      var lenth = snapshot.data.length;
                      if (lenth > 0) {
                        for (var i = 0; i < lenth; i++) {
                          ViewerModel v = snapshot.data[i];
                          people.add(Container(
                            padding: EdgeInsets.all(2),
                            child:  InitIconContainer(
                                    radius: (MediaQuery.of(context).size.width /
                                            2) /
                                        8,
                                    text: v.name,
                                  ),
                          ));
                        }
                      }
                    }



                    return Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          GestureDetector(
                            onTap: () {
                              widget.onView();
                            },
                            child: Container(
                              padding: EdgeInsets.all(2),
                              child:Image(
                                image: AssetImage("images/Viewer_list.png",),
                                width: 22,
                              )

                              // Icon(
                              //   Icons.remove_red_eye,
                              //   color: Color(0x88000000),
                              // ),
                            ),
                          ),
                          Container(
                            child: Wrap(
                              children: people,
                            ),
                          ),
                          GestureDetector(
                            onTap: (){
                              widget.onClose();
                            },
                            child: Container(
                              padding: EdgeInsets.all(2),
                                child:Image(
                                  image: AssetImage("images/end_live.png",),
                                  width: 24,
                                )
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class StarCoinViewer extends StatelessWidget {
  final String image;
  final String value;
  StarCoinViewer({this.image, this.value});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 3),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset("images/${image}"),
          SizedBox(
            width: 3,
          ),
          Text(
            value,
            style: TextStyle(
                color: Colors.white, fontSize: 10, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
