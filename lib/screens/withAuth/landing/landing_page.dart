import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hexagon/hexagon.dart';
import 'package:hive/screens/nearby/nearby_people.dart';
import 'package:hive/screens/shared/constants.dart';
import 'package:hive/screens/withAuth/go_live/current_live.dart';
import 'coming_soon.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  var primaryIdx = 1;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return DefaultTabController(
      initialIndex: 1,
      length: 5,
      child: Scaffold(
        // floatingActionButton: FloatingActionButton(
        //   onPressed: () {
        //     Navigator.of(context).pushNamed("/live_landing");
        //   },
        //   backgroundColor: Colors.white,
        //   child: HexagonWidget.flat(
        //     cornerRadius: 10,
        //     height: 30,
        //     width: 45,
        //     color: Color(0x00EFAA1F),
        //     padding: 4.0,
        //     child: Image(
        //       image: AssetImage("images/icon-05.png"),
        //     ),
        //   ),
        // ),
        // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        // bottomNavigationBar: SizedBox(
        //   height: 50,
        //   child: BottomNavigationBar(
        //     type: BottomNavigationBarType.shifting,
        //     onTap: (idx) {
        //       // if (idx == 2) {
        //       //   Navigator.of(context).pushNamed("/go_live_landing");
        //       // } else if (idx == 4) {
        //       //   Navigator.of(context).pushNamed("/profile");
        //       // }
        //     },
        //     iconSize: 10,
        //     showSelectedLabels: false,
        //     showUnselectedLabels: false,
        //     items: [
        //       BottomNavigationBarItem(
        //         label: "",
        //         icon: HexagonWidget.flat(
        //           cornerRadius: 10,
        //           height: 20,
        //
        //           color: Color(0xFFF1F1F2),
        //           padding: 4.0,
        //           child: Image(
        //             image: AssetImage("images/icon-01.png"),
        //           ),
        //         ),
        //       ),
        //       BottomNavigationBarItem(
        //         label: "",
        //         icon: HexagonWidget.flat(
        //           cornerRadius: 10,
        //           height: 20,
        //
        //           color: Color(0xFFF1F1F2),
        //           child: Image(
        //             image: AssetImage("images/icon-02.png"),
        //           ),
        //         ),
        //       ),
        //       BottomNavigationBarItem(
        //         label: "",
        //         icon:Icon(Icons.assistant_outlined,color: Colors.transparent,),
        //       ),
        //       BottomNavigationBarItem(
        //         label: "",
        //         icon: HexagonWidget.flat(
        //           cornerRadius: 10,
        //           height: 20,
        //
        //           color: Color(0xFFF1F1F2),
        //           padding: 4.0,
        //           child: Image(
        //             image: AssetImage("images/icon-03.png"),
        //           ),
        //         ),
        //       ),
        //       BottomNavigationBarItem(
        //         label: "",
        //         icon: HexagonWidget.flat(
        //           cornerRadius: 10,
        //           height: 20,
        //
        //           color: Color(0xFFF1F1F2),
        //           child: Image(
        //             image: AssetImage("images/icon-04.png"),
        //           ),
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
        appBar: AppBar(
          backgroundColor: Colors.white,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(0),
            child: Container(
              child: Row(
                children: [
                  Expanded(
                    flex: 5,
                    child: TabBar(

                      labelPadding: EdgeInsets.only(left: 10),
                      isScrollable: true,
                      indicatorColor: Color(0xFFBD2823),
                      onTap: (idx) {
                        setState(() {
                          primaryIdx = idx;
                        });
                      },
                      tabs: [

                        Tab(

                          child: LabelText(
                            selected: primaryIdx == 0,
                            label: "NEARBY",
                          ),

                        ),
                        LabelText(
                          selected: primaryIdx == 1,
                          label: "POPULAR",
                        ),
                        Tab(
                          child: LabelText(
                            selected: primaryIdx == 2,
                            label: "PARTY",
                          ),
                        ),
                        Tab(
                          child: LabelText(
                            selected: primaryIdx == 3,
                            label: "QUEEN",
                          ),
                        ),
                        Tab(
                          child: LabelText(
                            selected: primaryIdx == 4,
                            label: "PK",
                          ),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Row(
                      children: [
                        Image.asset("images/search-icon.png"),
                        SizedBox(
                          width: 10,
                        ),
                        Image.asset("images/notification-icon.png"),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        body: Stack(
          children: [

            TabBarView(
              children: [
                NearbyPeopleTab(),
                CurrentLive(
                  type: "both",
                ),
                CurrentLive(
                  type: "audio",
                ),
                // ComingSoon(),
                // ComingSoon(),
                // ComingSoon(),
                ComingSoon(),
                ComingSoon(),
              ],
            ),
            Positioned(
                bottom: 0,
                left: 0,
                child: Container(
                  width: size.width,
                  height: 70,
                  child: Stack(
                    overflow: Overflow.visible,
                    children: [
                      CustomPaint(
                        size: Size(size.width, 80),
                        painter: BNBCustomPainter(),
                      ),
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).pushNamed("/live_landing");
                        },
                        child: Center(
                          heightFactor: 1,
                          child: SvgPicture.asset("images/Live.svg",width: 50,)
                        ),
                      ),
                      Container(
                        width: size.width,
                        height: 80,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              padding: EdgeInsets.only(top: 20),
                              child: SvgPicture.asset("images/Explore_Active.svg",width: 26,)
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 20),
                              child: SvgPicture.asset(
                                "images/Bar_inactive.svg",
                                width: 30,
                              ),
                            ),
                            Container(
                              width: size.width * 0.20,
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 20),
                              child: SvgPicture.asset(
                                "images/Home_inactive.svg",
                                width: 30,
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 20),
                              child: SvgPicture.asset(
                                "images/Profile_inactive.svg",
                                width: 30,
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )),
          ],
        ),
      ),
    );
  }
}

class LabelText extends StatelessWidget {
  final String label;
  final bool selected;

  LabelText({this.label, this.selected});
  @override
  Widget build(BuildContext context) {
    return Text(
      label,
      style: selected ? selectedLableStyle : lableStyle,
    );
  }
}

class BNBCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    //
    // Path path_0 = Path();
    // path_0.moveTo(size.width*1.660597,size.height*2.155293);
    // path_0.lineTo(size.width*1.660597,size.height*1.514825);
    // path_0.lineTo(size.width*1.317796,size.height*1.515824);
    // path_0.cubicTo(size.width*1.279118,size.height*1.504372,size.width*1.253292,size.height*1.476432,size.width*1.237049,size.height*1.399085);
    // path_0.arcToPoint(Offset(size.width*1.109860,size.height*1.374157),radius: Radius.elliptical(size.width*0.08229159, size.height*0.3707084),rotation: 0 ,largeArc: false,clockwise: false);
    // path_0.lineTo(size.width*1.109855,size.height*1.374098);
    // path_0.cubicTo(size.width*1.109796,size.height*1.374474,size.width*1.109727,size.height*1.374792,size.width*1.109667,size.height*1.375158);
    // path_0.quadraticBezierTo(size.width*1.107761,size.height*1.384824,size.width*1.106008,size.height*1.395089);
    // path_0.cubicTo(size.width*1.089819,size.height*1.475529,size.width*1.063761,size.height*1.504174,size.width*1.024414,size.height*1.515826);
    // path_0.lineTo(size.width*0.6817613,size.height*1.515826);
    // path_0.lineTo(size.width*0.6817613,size.height*2.155293);
    // path_0.close();
    //
    // Paint paint = new Paint()
    //   ..color = Colors.black
    //   ..style = PaintingStyle.fill;
    // canvas.drawColor(Colors.black, BlendMode.);
    // canvas.drawPath(path_0,paint);
    Paint paint = new Paint()
      ..color = Colors.grey[50]
      ..style = PaintingStyle.fill;

    Path path = Path();
    path.moveTo(0, 30); // Start
    path.quadraticBezierTo(size.width * 0.20, 20, size.width * 0.35, 20);
    path.quadraticBezierTo(size.width * 0.40, 20, size.width * 0.40, 20);

    path.arcToPoint(Offset(size.width * 0.60, 20),
        radius: Radius.elliptical(30.0,25), clockwise: true);
    path.quadraticBezierTo(size.width * 0.60, 20, size.width * 0.65, 20);
    path.quadraticBezierTo(size.width * 0.80, 20, size.width, 30);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.lineTo(0, 20);
    canvas.drawShadow(path, Colors.black,10,false);
    canvas.drawColor(Colors.black, BlendMode.overlay);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
