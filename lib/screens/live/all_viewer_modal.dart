
import 'package:flutter/material.dart';
import 'package:hive/screens/live/pages/call.dart';

openAllViewerModal(
    BuildContext context, String channelName) {
  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    isDismissible: true,
    backgroundColor: Colors.transparent,
    builder: (context) {
      return Container(
          height: MediaQuery.of(context).size.height/2,
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30))),
          child: AllViewerList(channelName: channelName,)
        ),
      );
    },
  );


}
