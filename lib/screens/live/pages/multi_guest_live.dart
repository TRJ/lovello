import 'dart:async';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:agora_rtc_engine/rtc_engine.dart';

import 'package:flutter/material.dart';
import 'package:hive/models/chat_model.dart';
import 'package:hive/models/current_user_model.dart';
import 'package:hive/models/live_model.dart';
import 'package:hive/screens/live/all_viewer_modal.dart';

import 'package:hive/screens/live/close_live_bottom_modal.dart';
import 'package:hive/screens/live/pages/multi_guest_render_4.dart';
import 'package:hive/screens/live/pages/multi_guest_render_6.dart';
import 'package:hive/screens/live/utils/settings.dart';
import 'package:hive/screens/shared/bottom_scrollsheet_message.dart';
import 'package:hive/screens/shared/loading.dart';
import 'package:hive/screens/withAuth/go_live/live_header.dart';

import 'package:hive/services/firestore_services.dart';
import 'package:hive/services/timer_services.dart';
import 'package:hive/services/token_manager_services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:screen/screen.dart';

import 'multi_guest_render_9.dart';

class MultiGuestLive extends StatefulWidget {
  final String channelName;
  final bool broadcaster;
  final LiveModel broadcastUser;
  final int seats;
  MultiGuestLive({this.channelName,this.broadcaster = false,this.broadcastUser,this.seats});
  @override
  _MultiGuestLiveState createState() => _MultiGuestLiveState();
}

class _MultiGuestLiveState extends State<MultiGuestLive> {
  final _users = <int>[];
  final _infoStrings = <String>[];
  bool muted = false;
  RtcEngine _engine;

  TextEditingController _comment = TextEditingController();

  StreamSubscription _stream;
  int guest;
  TimerServices timerServices = TimerServices();
  CurrentUserModel user;
  bool connecting = false;
  String liveLoading = "Loading";
  Map<int, dynamic> guestInfos=  {};
  List<int> us = [10101,10102,10103,10104,10105,10106,10107,10108,10109];
  bool loading = true;
  @override
  void dispose() {

    _users.clear();

    _engine.leaveChannel();
    _engine.destroy();
    _comment.dispose();
    if(_stream!=null){
      _stream.cancel();
    }
    timerServices.close();
    disposeLive();
    super.dispose();
  }

  disposeLive() async {
    if (widget.broadcaster) {
      print("video dispose");
      await FirestoreServices.onWillPop(widget.channelName);
    }else{
      await FirestoreServices.onGuestLeave(widget.channelName);

    }
  }


  @override
  void initState() {
    super.initState();
    // initialize agora sdk
    initialize();
    if (!widget.broadcaster) {

      FirestoreServices.newUserJoined(widget.channelName);
    }
  }

  Future<void> initialize() async {
    user = await TokenManagerServices().getData();
    setState(() {
      loading = false;
    });
    _initialPermissionsAndInitialization();

    if(widget.broadcaster){

      initializeForBroadcaster(us[0]);
      timerServices.startTimer();
    }else{
      initializeForOthers();
    }

  }

  Future<void> _handlePermissions(Permission permission) async {
    final status = await permission.request();
    print(status);
  }

  _initialPermissionsAndInitialization() async {
    await _handlePermissions(Permission.camera);
    await _handlePermissions(Permission.microphone);
    bool isKeptOn = await Screen.isKeptOn;
    if(!isKeptOn){
      Screen.keepOn(true);
    }
    // await _localRenderer.initialize();
    // await _remoteRenderer.initialize();
  }

  Future<void> initializeForBroadcaster(int bid)async{

    connecting = true;

    _engine = await RtcEngine.create(APP_ID);
    await _engine.enableVideo();
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);

    await _engine.setClientRole( ClientRole.Broadcaster);

    _addAgoraEventHandlers();
    await _engine.enableWebSdkInteroperability(true);
    VideoEncoderConfiguration configuration = VideoEncoderConfiguration();
    configuration.dimensions = VideoDimensions(width: 1920,height: 1080);
    await _engine.setVideoEncoderConfiguration(configuration);
    await _engine.joinChannel(Token, widget.channelName, null, bid);


    // if(widget.broadcaster){
    //
    // }else{
    //   await _engine.joinChannel(Token, widget.channelName, null, 0);
    // }

  }

  Future<void> initializeForOthers()async{
    _engine = await RtcEngine.create(APP_ID);
    await _engine.enableVideo();
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    await _engine.setClientRole( ClientRole.Audience);
    _addAgoraEventHandlers();
    await _engine.joinChannel(Token, widget.channelName, null, 0);
  }

  /// Add agora event handlers
  void _addAgoraEventHandlers() {
    _engine.setEventHandler(RtcEngineEventHandler(error: (code) {
      setState(() {
        final info = 'onError: $code';
        _infoStrings.add(info);
      });
    }, joinChannelSuccess: (channel, uid, elapsed) {

      print(uid);
      setState(() {
        final info = 'onJoinChannel: $channel, uid: $uid';
        _infoStrings.add(info);
      });
    }, leaveChannel: (stats) {
      setState(() {
        _infoStrings.add('onLeaveChannel');
        _users.clear();
      });
    }, userJoined: (uid, elapsed)async {
      Map<int,dynamic> tempGuestInfo = await FirestoreServices.getMultiGuests(widget.channelName);

      setState(() {
        guestInfos = tempGuestInfo;
        final info = 'userJoined: $uid';
        connecting = false;
        _infoStrings.add(info);
        _users.add(uid);
        print(info);

      });
    }, userOffline: (uid, elapsed) {
      setState(() {
        final info = 'userOffline: $uid';
        _infoStrings.add(info);
        _users.remove(uid);
      });
    }, firstRemoteVideoFrame: (uid, width, height, elapsed) {
      setState(() {
        final info = 'firstRemoteVideo: $uid ${width}x $height';
        _infoStrings.add(info);
      });
    }));
  }

  Widget _panel() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      alignment: Alignment.bottomCenter,
      child: FractionallySizedBox(
        heightFactor: 0.25,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: StreamBuilder(
            stream: FirestoreServices().getChats(widget.channelName),
            builder: (context, snapshot) {
              
              if (!snapshot.hasData) {
                return Container();
              }
              List<ChatModel> chats = snapshot.data;
              chats.add(ChatModel(message: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",defaultMessage: true));
              
              return ListView.builder(
                reverse: true,
                itemCount: chats.length,
                itemBuilder: (context, i) {
                  ChatModel chat = chats[i];
                  if(chat.defaultMessage){
                    return Container(
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                      child: Text(chat.message,style: TextStyle(color: Colors.white),),
                    );
                  }
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                    child: Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: Colors.white),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 2),
                            child: Text(
                              "1+",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Text(
                          "${chat.name} :  ",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          chat.message,
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }

  _getRenderViews(){
    Widget primary;
    if(widget.broadcaster){
      primary = RtcLocalView.SurfaceView();
    }else{
      if(_users.contains(us[0])){
      setState(() {
        liveLoading="Live ended by broadcaster";
      });

        primary = RtcRemoteView.SurfaceView(uid: us[0]);
      }
    }

    Map<int,Widget> guests = {};
    if(guest !=null){
      guests[guest]  = RtcLocalView.SurfaceView();
    }

    _users.forEach((int uid){

      if(uid !=us[0]){
        guests[uid] = RtcRemoteView.SurfaceView(uid: uid);

      }

    });


    return [primary,guests];
  }

  Widget _viewRows(){
    final views = _getRenderViews();

    String primaryName = widget.broadcastUser!=null?widget.broadcastUser.name:user.name;
    if(views[0] !=null){
      if(widget.seats == 4){

        return MultiGuestRender4(primary: views[0],guest: views[1],us: us,guestInfo: guestInfos,primaryName: primaryName,
          onJoin: (val)async{
            if(!(_users.contains(val))){
              if(!widget.broadcaster && guest==null){
                FirestoreServices.storeMultiGuests(widget.channelName, val.toString());

                print("You can enter");
                guest = val;
                await  _engine.leaveChannel();
                initializeForBroadcaster(val);

              }else{
                print("You can not enter");
              }
            }
          },
        );
      }

     else if(widget.seats == 6){
        return Container(
          padding: EdgeInsets.only(top:70),
          child: MultiGuestRender6(primary: views[0],guest: views[1],us: us,guestInfo: guestInfos,primaryName: primaryName,
            onJoin: (val)async{
              if(!(_users.contains(val))){
                if(!widget.broadcaster && guest==null){
                  FirestoreServices.storeMultiGuests(widget.channelName, val.toString());

                  print("You can enter");
                  guest = val;
                  await  _engine.leaveChannel();
                  initializeForBroadcaster(val);

                }else{
                  print("You can not enter");
                }
              }
            },

          ),
        );
      }
      else if(widget.seats == 9){
        return Container(
          padding: EdgeInsets.only(top:70),
          child: MultiGuestRender9(primary: views[0],guest: views[1],us: us,guestInfo: guestInfos,primaryName: primaryName,
            onJoin: (val)async{
              if(!(_users.contains(val))){
                if(!widget.broadcaster && guest==null){
                  FirestoreServices.storeMultiGuests(widget.channelName, val.toString());
                  print("You can enter");
                  guest = val;
                  await  _engine.leaveChannel();
                 await initializeForBroadcaster(val);

                }
              }
            },

          ),
        );
      }
      return  Container();
    }else{
      return Container(
        child: Center(
          child: Text(
           connecting?"Connecting": liveLoading,
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
    }

  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        int seconds = timerServices.getCurrentTime();
        closeLiveModal(context,widget.channelName,seconds);
        return false;
      },
      child: Scaffold(
        backgroundColor: Color(0xFFF42408),
        body: SafeArea(
          child:loading?Loading(): Center(
            child: Stack(
              children: [



                Container(
                  // decoration: BoxDecoration(
                  //    ),
                  child: Column(
                    children: [

                      Expanded(
                        child: Container(
                          child: _panel(),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.1,
                        width: MediaQuery.of(context).size.width,
                       decoration: BoxDecoration(
                           gradient: LinearGradient(
                               colors: [Colors.transparent, Color(0xEE000000)],
                               begin: Alignment.topCenter,
                               end: Alignment.bottomCenter,
                              )
                       ),
                        child: Row(
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  IconButton(
                                      onPressed: () {
                                        openModalBottomSheetForMessage(context,widget.channelName,_comment);
                                      },
                                      icon: Icon(
                                        Icons.message_outlined,
                                        color: Colors.white,
                                      )),


                                ],
                              ),
                            )
                          ],
                        ),
                      ),

                    ],
                  ),
                ),

                _viewRows(),

                Container(
                  alignment: Alignment.topRight,
                  child: Container(
                    child: IconButton(
                      onPressed: (){

                        if(widget.broadcaster){
                          int seconds = timerServices.getCurrentTime();
                        //  print(seconds);
                         closeLiveModal(context,widget.channelName,seconds);
                          //timerServices.close();
                        }else{
                          Navigator.of(context).pop();
                        }

                      },
                      icon: Icon(Icons.cancel,color: Color(0x88000000),),
                    ),
                  ),
                ),
                LiveHeader(
                    broadcastUser: widget.broadcastUser,
                    broadcaster: widget.broadcaster,
                    channelName: widget.channelName,
                    onClose:(){
                      if(widget.broadcaster){
                        int seconds = timerServices.getCurrentTime();
                        closeLiveModal(context,widget.channelName,seconds);
                        //timerServices.close();
                      }else{
                        Navigator.of(context).pop();
                      }
                    },
                    onView:(){
                      openAllViewerModal(context,widget.channelName);
                    }
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
