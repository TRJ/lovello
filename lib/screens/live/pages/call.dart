import 'dart:async';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hive/models/chat_model.dart';
import 'package:hive/models/current_user_model.dart';
import 'package:hive/models/live_model.dart';
import 'package:hive/models/viewer_model.dart';
import 'package:hive/screens/live/all_viewer_modal.dart';
import 'package:hive/screens/live/close_live_bottom_modal.dart';
import 'package:hive/screens/shared/bottom_scrollsheet_message.dart';
import 'package:hive/screens/shared/initIcon_container.dart';
import 'package:hive/screens/shared/network_image.dart';
import 'package:hive/screens/withAuth/go_live/live_header.dart';
import 'package:hive/services/firestore_services.dart';
import 'package:hive/services/timer_services.dart';
import 'package:hive/services/token_manager_services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:screen/screen.dart';

import '../utils/settings.dart';

class CallPage extends StatefulWidget {
  /// non-modifiable channel name of the page
  final String channelName;

  /// non-modifiable client role of the page
  final int role;
  final bool broadcaster;
  final LiveModel broadcastUser;

  /// Creates a call page with given channel name.
  const CallPage(
      {Key key, this.channelName, this.role,this.broadcastUser, this.broadcaster = false})
      : super(key: key);

  @override
  _CallPageState createState() => _CallPageState();
}

class _CallPageState extends State<CallPage> {
  final _users = <int>[];
  final _infoStrings = <String>[];
  bool muted = false;
  RtcEngine _engine;

  TextEditingController _comment = TextEditingController();

  StreamSubscription _stream;
  bool guest = false;
  TimerServices timerServices = TimerServices();
  String liveLoading = "Loading";

  @override
  void dispose() {

    _users.clear();

    _engine.leaveChannel();
    _engine.destroy();
    _comment.dispose();
    if(_stream!=null){
      _stream.cancel();
    }
    timerServices.close();
    disposeLive();
    super.dispose();
  }

  disposeLive() async {
    if (widget.broadcaster) {
      print("video dispose");
      await FirestoreServices.onWillPop(widget.channelName);
    }else{
      await FirestoreServices.onGuestLeave(widget.channelName);
    }
  }


  @override
  didChangeAppLifecycleState(AppLifecycleState state) {
    print(state);
    print("here");
    if (state == AppLifecycleState.paused) {
      print("HERE");
      disposeLive();
    }

  }

  @override
  void initState() {
    super.initState();
    // initialize agora sdk
    initialize();
    if (!widget.broadcaster) {

      FirestoreServices.newUserJoined(widget.channelName);
    }
  }

  Future<void> initialize() async {
    _initialPermissionsAndInitialization();

    if(widget.broadcaster){

      initializeForBroadcaster();
      timerServices.startTimer();
    }else{
      initializeForOthers();
    }

  }
  Future<void> _handlePermissions(Permission permission) async {
    final status = await permission.request();
    print(status);
  }

  _initialPermissionsAndInitialization() async {
    await _handlePermissions(Permission.camera);
    await _handlePermissions(Permission.microphone);
    bool isKeptOn = await Screen.isKeptOn;
    if(!isKeptOn){
      Screen.keepOn(true);
    }
    // await _localRenderer.initialize();
    // await _remoteRenderer.initialize();
  }
  Future<void> initializeForBroadcaster()async{



    _engine = await RtcEngine.create(APP_ID);
    await _engine.enableVideo();
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);

    await _engine.setClientRole( ClientRole.Broadcaster);

    _addAgoraEventHandlers();
    await _engine.enableWebSdkInteroperability(true);
    VideoEncoderConfiguration configuration = VideoEncoderConfiguration();
    configuration.dimensions = VideoDimensions(width: 1920,height: 1080);
    await _engine.setVideoEncoderConfiguration(configuration);
    if(widget.broadcaster){
      await _engine.joinChannel(Token, widget.channelName, null, 10010);
    }else{
      await _engine.joinChannel(Token, widget.channelName, null, 0);
    }

  }

  Future<void> initializeForOthers()async{
    _engine = await RtcEngine.create(APP_ID);
    await _engine.enableVideo();
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    await _engine.setClientRole( ClientRole.Audience);
    _addAgoraEventHandlers();
    await _engine.joinChannel(Token, widget.channelName, null, 0);
  }



  /// Add agora event handlers
  void _addAgoraEventHandlers() {
    _engine.setEventHandler(RtcEngineEventHandler(error: (code) {
      setState(() {
        final info = 'onError: $code';
        _infoStrings.add(info);
      });
    }, joinChannelSuccess: (channel, uid, elapsed) {

      print(uid);
      setState(() {
        final info = 'onJoinChannel: $channel, uid: $uid';
        _infoStrings.add(info);
      });
    }, leaveChannel: (stats) {
      setState(() {
        _infoStrings.add('onLeaveChannel');
        _users.clear();
      });
    }, userJoined: (uid, elapsed) {
      setState(() {
        final info = 'userJoined: $uid';
        _infoStrings.add(info);
        _users.add(uid);
      });
    }, userOffline: (uid, elapsed) {
      setState(() {
        final info = 'userOffline: $uid';
        _infoStrings.add(info);
        _users.remove(uid);
      });
    }, firstRemoteVideoFrame: (uid, width, height, elapsed) {
      setState(() {
        final info = 'firstRemoteVideo: $uid ${width}x $height';
        _infoStrings.add(info);
      });
    }));
  }

  /// Helper function to get list of native views
   _getRenderViews() {
    Widget primary;
    if (widget.broadcaster) {
      primary = RtcLocalView.SurfaceView();
    }else{
      if(_users.contains(10010)){
        setState(() {
          liveLoading="Live ended by broadcaster";
        });
        primary = RtcRemoteView.SurfaceView(uid: 10010);
      }
    }
    List<Widget> guests = [];

    if(guest){
      guests.add(Container(
        height: 150,
        width: 100,
        padding: EdgeInsets.only(top: 20),
        child: RtcLocalView.SurfaceView(),
      ));
    }
    _users.forEach((int uid){

      if(uid !=10010){
        guests.add(Container(
          height: 150,
          width: 100,
          padding: EdgeInsets.only(top: 20),
          child: RtcRemoteView.SurfaceView(uid: uid),
        ));

      }

    });
    return [primary,guests];
  }

  /// Video view wrapper
  Widget _videoView(view) {
    return Expanded(child: Container(child: view));
  }

  /// Video view row wrapper
  Widget _expandedVideoRow(List<Widget> views) {
    final wrappedViews = views.map<Widget>(_videoView).toList();
    return Expanded(
      child: Row(
        children: wrappedViews,
      ),
    );
  }

  /// Video layout wrapper
  Widget _viewRows() {
    final views = _getRenderViews();
    if(views[0]!=null){
      return(
        Stack(
          children: [
            views[0],
           views[1].length>0? Positioned(
              bottom: 100,
              right: 30,
              child: Column(
                children: views[1],
              ),
            ):Container()
          ],
        )
      );
    }else{
      return Container(
        child: Center(
          child: Text(
          "$liveLoading",
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
    }
    // // return Container(
    // //     child: Column(
    // //       children: <Widget>[_videoView(views[0])],
    // //     ));
    // switch (views.length) {
    //   case 1:
    //     return Container(
    //         child: Column(
    //       children: <Widget>[_videoView(views[0])],
    //     ));
    //   case 2:
    //     return Container(
    //         child: Column(
    //       children: <Widget>[
    //         _expandedVideoRow([views[0]]),
    //         _expandedVideoRow([views[1]])
    //       ],
    //     ));
    //   case 3:
    //     return Container(
    //         child: Column(
    //       children: <Widget>[
    //         _expandedVideoRow(views.sublist(0, 2)),
    //         _expandedVideoRow(views.sublist(2, 3))
    //       ],
    //     ));
    //   case 4:
    //     return Container(
    //         child: Column(
    //       children: <Widget>[
    //         _expandedVideoRow(views.sublist(0, 2)),
    //         _expandedVideoRow(views.sublist(2, 4))
    //       ],
    //     ));
    //   default:
    // }

  }

  /// Toolbar layout
  Widget _toolbar() {
    if (widget.role != 1) return Container();
    return Container(
      alignment: Alignment.bottomCenter,
      padding: const EdgeInsets.symmetric(vertical: 48),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RawMaterialButton(
            onPressed: _onToggleMute,
            child: Icon(
              muted ? Icons.mic_off : Icons.mic,
              color: muted ? Colors.white : Colors.blueAccent,
              size: 20.0,
            ),
            shape: CircleBorder(),
            elevation: 2.0,
            fillColor: muted ? Colors.blueAccent : Colors.white,
            padding: const EdgeInsets.all(12.0),
          ),
          RawMaterialButton(
            onPressed: () => _onCallEnd(context),
            child: Icon(
              Icons.call_end,
              color: Colors.white,
              size: 35.0,
            ),
            shape: CircleBorder(),
            elevation: 2.0,
            fillColor: Colors.redAccent,
            padding: const EdgeInsets.all(15.0),
          ),
          RawMaterialButton(
            onPressed: _onSwitchCamera,
            child: Icon(
              Icons.switch_camera,
              color: Colors.blueAccent,
              size: 20.0,
            ),
            shape: CircleBorder(),
            elevation: 2.0,
            fillColor: Colors.white,
            padding: const EdgeInsets.all(12.0),
          )
        ],
      ),
    );
  }

  /// Info panel to show logs
  Widget _panel() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      alignment: Alignment.bottomCenter,
      child: FractionallySizedBox(
        heightFactor: 0.25,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: StreamBuilder(
            stream: FirestoreServices().getChats(widget.channelName),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Container();
              }
              List<ChatModel> chats = snapshot.data;
              return ListView.builder(
                reverse: true,
                itemCount: chats.length,
                itemBuilder: (context, i) {
                  ChatModel chat = chats[i];
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                    child: Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: Colors.white),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 2),
                            child: Text(
                              "1+",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Text(
                          "${chat.name} :  ",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          chat.message,
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }

  void _onCallEnd(BuildContext context) async {
    Navigator.pop(context);
  }

  void _onToggleMute() {
    setState(() {
      muted = !muted;
    });
    _engine.muteLocalAudioStream(muted);
  }

  void _onSwitchCamera() {
    _engine.switchCamera();
  }

  void onStartListen()async{
    CurrentUserModel user =await TokenManagerServices().getData();
    _stream =   await FirebaseFirestore.instance.collection("live_streams").doc(widget.channelName).collection("viewers").where('status',whereIn: [3,4]).snapshots().listen((QuerySnapshot q) {
      if(q.docs.length>0){
        q.docs.forEach((d) async{
          if(user.userId == d.id){
           guest = true;
          await  _engine.leaveChannel();
          initializeForBroadcaster();
          }
        });
      }
    });
  }



  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        int seconds = timerServices.getCurrentTime();
        closeLiveModal(context,widget.channelName,seconds);
        return false;
      },
      child: Scaffold(
        backgroundColor: Color(0xFFF42408),
        body: SafeArea(
          child: Center(
            child: Stack(
              children: <Widget>[

                _viewRows(),
                Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Colors.transparent, Color(0xEE000000)],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          stops: [.5, 1])),
                  child: Column(
                    children: [
                      // Container(
                      //   alignment: Alignment.topRight,
                      //   child: Container(
                      //     child: IconButton(
                      //       onPressed: (){
                      //
                      //         if(widget.broadcaster){
                      //           int seconds = timerServices.getCurrentTime();
                      //           closeLiveModal(context,widget.channelName,seconds);
                      //           //timerServices.close();
                      //         }else{
                      //           Navigator.of(context).pop();
                      //         }
                      //
                      //       },
                      //       icon: Icon(Icons.cancel,color: Color(0x88000000),),
                      //     ),
                      //   ),
                      // ),
                      Expanded(
                        child: Container(
                          child: _panel(),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.1,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                //colors: [Colors.transparent, Color(0xEE000000)],
                                colors: [Colors.transparent, Colors.black],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                //stops: [.5, 1]
                              )
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  IconButton(
                                      onPressed: () {
                                        openModalBottomSheetForMessage(context,widget.channelName,_comment);
                                      },
                                    icon:Image.asset("images/Chat.png")

                                  ),
                                  IconButton(
                                      onPressed: () {

                                      },
                                      icon:Image.asset("images/addons.png")

                                  ),

                                  IconButton(
                                      onPressed: () {

                                      },
                                      icon:Image.asset("images/Gift.png")

                                  ),
                                  IconButton(
                                      onPressed: () {
                                        showModalBottomSheet(
                                            context: context,
                                            isScrollControlled: true,
                                            isDismissible: false,
                                            backgroundColor: Colors.transparent,
                                            builder: (context) {
                                              return StatefulBuilder(
                                                  builder: (context, state) {
                                                return DraggableScrollableSheet(
                                                  expand: false,
                                                  initialChildSize: .6,
                                                  minChildSize: .6,
                                                  maxChildSize: .6,
                                                  builder: (context,
                                                      scrollController) {
                                                    return Container(
                                                        decoration: BoxDecoration(
                                                            color: Colors.white,
                                                            borderRadius:
                                                                BorderRadius.only(
                                                                    topLeft: Radius
                                                                        .circular(
                                                                            30),
                                                                    topRight: Radius
                                                                        .circular(
                                                                            30))),
                                                        child:
                                                            DefaultTabController(
                                                          length: 2,
                                                          child: Column(
                                                            children: [
                                                              Container(
                                                                padding: EdgeInsets.only(left: 50),
                                                                height: MediaQuery.of(context).size.height *.07,
                                                                child: Row(
                                                                  children: [
                                                                    Expanded(
                                                                        child:TabBar(
                                                                             labelStyle: TextStyle( color: Colors.red, fontWeight:FontWeight.w600,fontSize:17),
                                                                             unselectedLabelStyle: TextStyle(color: Colors.black,fontWeight:FontWeight.w600,fontSize:17),
                                                                             indicatorColor:Colors.red,
                                                                             labelColor:Colors.red,
                                                                             unselectedLabelColor:Colors.black,
                                                                             tabs: [
                                                                                  Container(
                                                                                    child: Text("WAITING"),
                                                                                  ),
                                                                                  Container(
                                                                                    child: Text("GUEST LIVE"),
                                                                                  )
                                                                                  ],
                                                                    )),
                                                                    Container(
                                                                      width: 60,
                                                                      child: IconButton(
                                                                        onPressed: (){
                                                                            Navigator.of(context).pop();
                                                                        },
                                                                        icon:Icon( Icons.clear),
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                              Expanded( child:Container(
                                                                          padding: EdgeInsets.only(left: 20),
                                                                          child: TabBarView(
                                                                              children: [
                                                                                VideoLiveWaitingList(channelName:widget.channelName,broadcaster: widget.broadcaster,),
                                                                                VideoLiveGuestList(channelName:widget.channelName,broadcaster: widget.broadcaster,)
                                                                              ],
                                                                            ),
                                                              )),
                                                              widget.broadcaster
                                                                  ? Container()
                                                                  : AudioVideoJoin(channelName: widget.channelName,onStartListen:onStartListen ,)
                                                            ],
                                                          ),
                                                        ));
                                                  },
                                                );
                                              });
                                            });
                                      },
                                      icon:Image.asset("images/Join_live_list.png")
                                  ),

                                  IconButton(
                                      onPressed: () {

                                      },
                                      icon:Image.asset("images/Flip.png")

                                  ),
                                  IconButton(
                                      onPressed: () {

                                      },
                                      icon:Image.asset("images/Video_turn_off.png")

                                  ),

                                ],
                              ),
                            ),

                            IconButton(
                                onPressed: () {

                                },
                                icon:Image.asset("images/PK.png")

                            ),
                          ],
                        ),
                      ),

                    ],
                  ),
                ),
                LiveHeader(
                  broadcastUser: widget.broadcastUser,
                  broadcaster: widget.broadcaster,
                  channelName: widget.channelName,
                  onClose:(){
                    if(widget.broadcaster){
                      int seconds = timerServices.getCurrentTime();
                      closeLiveModal(context,widget.channelName,seconds);
                      //timerServices.close();
                    }else{
                      Navigator.of(context).pop();
                    }
                  },
                  onView:(){
                    openAllViewerModal(context,widget.channelName);
                   }
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}

class AudioVideoJoin extends StatefulWidget {
  final String channelName;
  final Function onStartListen;
  AudioVideoJoin({this.channelName,this.onStartListen});
  @override
  _AudioVideoJoinState createState() => _AudioVideoJoinState();
}

class _AudioVideoJoinState extends State<AudioVideoJoin> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * .1,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          RaisedButton(
            onPressed: () async{
             await FirestoreServices.joinVideo(1, widget.channelName);
              widget.onStartListen();
            },
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
                side: BorderSide(color: Colors.red)),
            color: Colors.white,
            child: Row(
              children: [
                Icon(
                  Icons.mic_none_outlined,
                  color: Colors.black,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "Audio Join",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                )
              ],
            ),
          ),
          RaisedButton(
            onPressed: () async{
            await  FirestoreServices.joinVideo(2, widget.channelName);
            widget.onStartListen();
            },
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            color: Colors.red,
            child: Row(
              children: [
                Icon(
                  Icons.videocam,
                  color: Colors.white,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "Video Join",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.white),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class VideoLiveWaitingList extends StatefulWidget {
  final String channelName;
  final bool broadcaster;
  VideoLiveWaitingList({this.channelName, this.broadcaster});

  @override
  _VideoLiveWaitingListState createState() => _VideoLiveWaitingListState();
}

class _VideoLiveWaitingListState extends State<VideoLiveWaitingList> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: StreamBuilder(
      stream: FirestoreServices().getVideoWaitingViewer(widget.channelName),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Container();
        }
        List<ViewerModel> viewers = snapshot.data;
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 30),
              child: Text(
                "Waiting List (${snapshot.data.length})",
                style: TextStyle(color: Colors.grey[600]),
              ),
            ),
            Container(
                child: ListView.separated(
              shrinkWrap: true,
              primary: false,
              itemCount: viewers.length,
              itemBuilder: (context, i) {
                ViewerModel viewer = viewers[i];
                print(viewer);
                return ListTile(
                  leading: viewer.photoUrl !=null ? CircleAvatar(
                  radius: 20,
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(40),
                      child: HexagonProfilePicNetworkImage(
                        url: viewer.photoUrl,
                      )),
                )
                    : InitIconContainer(
                radius: 40,
                text: viewer.name,
                ),
                  title:Text(viewer.name,style: TextStyle(fontSize: 14),),

                trailing: widget.broadcaster?  Container(
                  width: 90,
                  height: 25,
                  child: RaisedButton(

                    onPressed: () {

                      FirestoreServices.addUserToVideoLive(viewer, widget.channelName);
                     // CallServices.joinVideo(2, widget.channelName);
                    },
                    shape:
                    RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                    color: Colors.red,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(
                         viewer.status==1? Icons.mic_none_outlined:Icons.videocam,
                          color: Colors.white,
                          size: 15,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          "Accept",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 12,
                              color: Colors.white),
                        )
                      ],
                    ),
                  ),
                ):Container( width: 1,),
                );
              },
                  separatorBuilder: (context,i){
                return Divider(thickness: 1,);
                  },
            ),)
          ],
        );
      },
    ));
  }
}

class VideoLiveGuestList extends StatefulWidget {

  final String channelName;
  final bool broadcaster;
  VideoLiveGuestList({this.channelName, this.broadcaster});

  @override
  _VideoLiveGuestListState createState() => _VideoLiveGuestListState();
}

class _VideoLiveGuestListState extends State<VideoLiveGuestList> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: StreamBuilder(
          stream: FirestoreServices().getVideoGuestViewer(widget.channelName),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Container();
            }
            List<ViewerModel> viewers = snapshot.data;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: Text(
                    "Guest List (${snapshot.data.length})",
                    style: TextStyle(color: Colors.grey[600]),
                  ),
                ),
                Container(
                    child: ListView.separated(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: viewers.length,
                      itemBuilder: (context, i) {
                        ViewerModel viewer = viewers[i];
                        print(viewer);
                        return ListTile(
                          leading:viewer.photoUrl !=null ? CircleAvatar(
                            radius: 20,
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(40),
                                child: HexagonProfilePicNetworkImage(
                                  url: viewer.photoUrl,
                                )),
                          )
                              : InitIconContainer(
                            radius: 40,
                            text: viewer.name,
                          ),
                          title: Text(viewer.name,style: TextStyle(fontSize: 14),)
                        );
                      },
                      separatorBuilder: (context,i){
                        return Divider(thickness: 1,);
                      },
                    ))
              ],
            );
          },
        ));
  }
}


class AllViewerList extends StatefulWidget {

  final String channelName;

  AllViewerList({this.channelName});

  @override
  _AllViewerListState createState() => _AllViewerListState();
}

class _AllViewerListState extends State<AllViewerList> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: StreamBuilder(
          stream: FirestoreServices().getVideoViewers(widget.channelName),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Container();
            }
            List<ViewerModel> viewers = snapshot.data;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 30,top: 10),
                  child: Text(
                    "Viewer List (${snapshot.data.length})",
                    style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold),
                  ),
                ),
                Divider(),
                Container(
                    child: ListView.separated(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: viewers.length,
                      itemBuilder: (context, i) {
                        ViewerModel viewer = viewers[i];
                        return ListTile(
                          dense: true,
                            leading:viewer.photoUrl !=null ? CircleAvatar(
                              radius: 20,
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(40),
                                  child: HexagonProfilePicNetworkImage(
                                    url: viewer.photoUrl,
                                  )),
                            )
                                : InitIconContainer(
                              radius: 40,
                              text: viewer.name,
                            ),
                            title: Text(viewer.name,style: TextStyle(fontSize: 14),)
                        );
                      },
                      separatorBuilder: (context,i){
                        return Divider(thickness: 1,);
                      },
                    ))
              ],
            );
          },
        ));
  }
}
