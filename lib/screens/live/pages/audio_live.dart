import 'dart:async';
import 'dart:convert';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter/material.dart';
import 'package:hive/models/chat_model.dart';
import 'package:hive/models/current_user_model.dart';
import 'package:hive/models/live_model.dart';
import 'package:hive/models/viewer_model.dart';
import 'package:hive/screens/live/all_viewer_modal.dart';
import 'package:hive/screens/live/close_live_bottom_modal.dart';
import 'package:hive/screens/shared/bottom_scrollsheet_message.dart';
import 'package:hive/screens/shared/initIcon_container.dart';
import 'package:hive/screens/shared/loading.dart';
import 'package:hive/screens/shared/network_image.dart';
import 'package:hive/screens/withAuth/go_live/live_header.dart';
import 'package:hive/services/firestore_services.dart';
import 'package:hive/services/token_manager_services.dart';
import 'package:ionicons/ionicons.dart';

import 'package:permission_handler/permission_handler.dart';
import 'package:screen/screen.dart';
import '../../../services/timer_services.dart';
import '../utils/settings.dart';

class AudioLive extends StatefulWidget {
  /// non-modifiable channel name of the page
  final String channelName;
  final LiveModel broadcastUser;
  final bool broadcaster;

  /// Creates a call page with given channel name.
  const AudioLive(
      {Key key, this.channelName, this.broadcaster = false,this.broadcastUser})
      : super(key: key);

  @override
  _AudioLiveState createState() => _AudioLiveState();
}

class _AudioLiveState extends State<AudioLive> with WidgetsBindingObserver {
  final _users = <int>[];
  final _infoStrings = <String>[];
  bool muted = false;
  RtcEngine _engine;

  TextEditingController _comment = TextEditingController();
  CurrentUserModel user;
  StreamSubscription _stream;
  bool guest = false;
  bool broadcasterLeft= false;

  int existingLive = 0;
  TimerServices timerServices = TimerServices();

  bool loading = true;
  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);

    _users.clear();

    _engine.leaveChannel();
    _engine.destroy();
    _comment.dispose();
    if(_stream!=null){
      _stream.cancel();
    }
    timerServices.close();
    disposeLive();

  }

  disposeLive() async {
    if (widget.broadcaster) {
      print("video dispose");
      await FirestoreServices.onWillPop(widget.channelName);
    }else{
      await FirestoreServices.onGuestLeave(widget.channelName);

    }

    WidgetsBinding.instance.addObserver(this);
  }

  @override
  didChangeAppLifecycleState(AppLifecycleState state) {
    print(state);
    print("here");
    if (state == AppLifecycleState.paused) {
      print("HERE");
      disposeLive();
    }

  }

  @override
  void initState() {
    super.initState();

    // initialize agora sdk
    initialize();
    if (!widget.broadcaster) {
      FirestoreServices.newUserJoined(widget.channelName);
      onStartListen();
    }
  }



  Future<void> initialize() async {
     user = await TokenManagerServices().getData();
     setState(() {
       loading = false;
     });
    _initialPermissionsAndInitialization();

    if(widget.broadcaster){
      initializeForBroadcaster();
      timerServices.startTimer();
    }else{
      initializeForOthers();
    }

  }
  Future<void> _handlePermissions(Permission permission) async {
    final status = await permission.request();
    print(status);
  }

  _initialPermissionsAndInitialization() async {
    await _handlePermissions(Permission.camera);
    await _handlePermissions(Permission.microphone);
    bool isKeptOn = await Screen.isKeptOn;
    if(!isKeptOn){
      Screen.keepOn(true);
    }
    // await _localRenderer.initialize();
    // await _remoteRenderer.initialize();
  }
  Future<void> initializeForBroadcaster()async{



    _engine = await RtcEngine.create(APP_ID);
    await _engine.enableAudio();
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);

    await _engine.setClientRole( ClientRole.Broadcaster);

    _addAgoraEventHandlers();
    await _engine.enableWebSdkInteroperability(true);
    VideoEncoderConfiguration configuration = VideoEncoderConfiguration();
    configuration.dimensions = VideoDimensions(width: 1920,height: 1080);
    await _engine.setVideoEncoderConfiguration(configuration);




    if(widget.broadcaster){
      await _engine.joinChannel(Token, widget.channelName, null, 10010);
    }else{
      await _engine.joinChannel(Token, widget.channelName, null, 0);
    }

  }

  Future<void> initializeForOthers()async{
    _engine = await RtcEngine.create(APP_ID);
    await _engine.enableAudio();
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    await _engine.setClientRole( ClientRole.Audience);
    _addAgoraEventHandlers();
    await _engine.joinChannel(Token, widget.channelName, null, 0);
  }



  /// Add agora event handlers
  void _addAgoraEventHandlers() {
    _engine.setEventHandler(RtcEngineEventHandler(error: (code) {
      setState(() {
        final info = 'onError: $code';
        _infoStrings.add(info);
      });
    }, joinChannelSuccess: (channel, uid, elapsed) {

      print(uid);
      setState(() {
        final info = 'onJoinChannel: $channel, uid: $uid';
        _infoStrings.add(info);
      });
    }, leaveChannel: (stats) {
      setState(() {
        _infoStrings.add('onLeaveChannel');
        _users.clear();
      });
    }, userJoined: (uid, elapsed) {
      setState(() {
        final info = 'userJoined: $uid';
        _infoStrings.add(info);
        _users.add(uid);
      });
    }, userOffline: (uid, elapsed) {
      setState(() {
        final info = 'userOffline: $uid';
        if(uid==10010){
          broadcasterLeft = true;
        }
        _infoStrings.add(info);
        _users.remove(uid);
      });
    }, firstRemoteVideoFrame: (uid, width, height, elapsed) {
      setState(() {
        final info = 'firstRemoteVideo: $uid ${width}x $height';
        _infoStrings.add(info);
      });
    }));
  }

  Widget getBroadcasterView(){
    Widget bUser;
    if(widget.broadcaster){




      bUser = Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*.12,left: 20,right: 20),
        child: Column(
          children: [
            user.image != null
                ? CircleAvatar(
              radius: MediaQuery.of(context).size.width*.18,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(40),
                  child: HexagonProfilePicNetworkImage(
                    url:  user.image,
                  )),
            )
                : InitIconContainer(
              radius: MediaQuery.of(context).size.width*.36,
              text: user.name,
            ),

          Padding(

            padding: const EdgeInsets.all(15.0),
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 4),
                decoration: BoxDecoration(
                  color: Color(0x66000000),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.volume_up_sharp,color: Colors.white,),
                    SizedBox(width: 5,),
                    RichText(text:  TextSpan(
                      text: "${user.name}",
                      style: TextStyle(fontSize: 14,color: Colors.white,fontWeight: FontWeight.w600),

                        children: [

                          TextSpan(text: "'s Stream",style: TextStyle(fontSize: 14,color: Colors.white),)
                        ]
                    ),)
                  ],
                )
            ),
          )

          ],
        ),
      );

    }else{
      bUser = Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*.11,left: 20,right: 20),
        child: Column(
         // mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
        widget.broadcastUser.photoUrl!=null?  CircleAvatar(
              backgroundColor:Colors.black12,
              radius: MediaQuery.of(context).size.width*.13,
              backgroundImage: CachedNetworkImageProvider(
                  widget.broadcastUser.photoUrl
              )
            ):InitIconContainer(
          radius: MediaQuery.of(context).size.width*.26,
          text: widget.broadcastUser.name,
        ),

            Padding(

              padding: const EdgeInsets.all(15.0),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 4),
                decoration: BoxDecoration(
                    color: Color(0x66000000),
                    borderRadius: BorderRadius.circular(20),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.volume_up_sharp,color: Colors.white,),
                    SizedBox(width: 5,),
                    RichText(text: TextSpan(
                       text: " ",
                        style: TextStyle(fontSize: 14,color: Colors.white),
                      children: [
                        TextSpan(
                          text: "${widget.broadcastUser.name}",
                          style: TextStyle(fontSize: 14,color: Colors.white,fontWeight: FontWeight.w600),
                        ),
                        TextSpan(text: "'s Stream",style: TextStyle(fontSize: 14,color: Colors.white),)
                      ]
                    ),)
                  ],
                )
              ),
            )
          ],
        ),
      );
    }

    return bUser;
  }
  Widget _viewRows() {

    Widget broadcaseUserView = getBroadcasterView();

    return Container(child: Column(
      children: [
        broadcaseUserView,
        StreamBuilder(
          stream:FirestoreServices().getVideoGuestViewer(widget.channelName),
          builder: (context,snapshot) {
            if(snapshot.hasData){
                print(snapshot.data);
                List<Widget> list = [];

                  List<ViewerModel> viewers = snapshot.data;
                  for(var i=0;i<viewers.length;i++){

                    ViewerModel viewer = viewers[i];
                    list.add(
                        SizedBox(
                          width: MediaQuery.of(context).size.width/4,
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 10,vertical: 3),
                            child: Column(
                              children: [
                            viewer.photoUrl!=null?  CircleAvatar(

                                  backgroundImage:NetworkImage(viewer.photoUrl),
                                  radius: MediaQuery.of(context).size.width*.08,
                                  backgroundColor: Colors.black12,
                                ):InitIconContainer(
                                radius: MediaQuery.of(context).size.width*.16,
                                text: viewer.name,
                            ),
                                SizedBox(height: 7,),
                                Text(viewer.name,maxLines: 1,overflow: TextOverflow.ellipsis,style: TextStyle(color: Colors.white),)
                              ],
                            ),
                          ),
                        )
                    );

                  }

                  existingLive = list.length;
              //  print(8-list.length);
                if(list.length<8){
                  int currentLenght = list.length;
                  int plus= 8-currentLenght;
                  for(var i=0;i<plus;i++){
                    print(i);
                    int serialNum = i+currentLenght+1;
                    list.add(

                        SizedBox(
                          width: MediaQuery.of(context).size.width/4,
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 10,vertical: 3),
                            child: Column(
                              children: [
                                CircleAvatar(
                                  radius: MediaQuery.of(context).size.width*.08,
                                  backgroundColor: Colors.black26,
                                  child: Center(
                                    child: Icon(Icons.add,color: Colors.white,),
                                  ),
                                ),
                                SizedBox(height: 7,),
                                Text(serialNum.toString(),maxLines: 1,overflow: TextOverflow.ellipsis,style: TextStyle(color: Colors.white),)
                              ],
                            ),
                          ),
                        )
                    );
                  }
                }

              return Container(

                  child: Wrap(
                children: list,
              ));
            }else{
              return Text(" ");
            }
          },

        )
      ],
    ));
  }


  /// Info panel to show logs
  Widget _panel() {
    return Container(
   //   padding: const EdgeInsets.symmetric(vertical: 5),
      alignment: Alignment.bottomCenter,
      child: FractionallySizedBox(
        heightFactor: 0.22,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: StreamBuilder(
            stream: FirestoreServices().getChats(widget.channelName),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Container();
              }
              List<ChatModel> chats = snapshot.data;
              return ListView.builder(
                reverse: true,
                itemCount: chats.length,
                itemBuilder: (context, i) {
                  ChatModel chat = chats[i];
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                    child: Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: Colors.white),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 2),
                            child: Text(
                              "1+",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Text(
                          "${chat.name} :  ",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          chat.message,
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }



  void onStartListen()async{


    _stream =   await FirebaseFirestore.instance.collection("live_streams").doc(widget.channelName).collection("viewers").where('status',whereIn: [3,4]).snapshots().listen((QuerySnapshot q) {
      if(q.docs.length>0){
        q.docs.forEach((d) async{
          if(user.userId == d.id){
            guest = true;
            await  _engine.leaveChannel();
            initializeForBroadcaster();
          }
        });
      }
    });
  }



  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        int seconds = timerServices.getCurrentTime();
        closeLiveModal(context,widget.channelName,seconds);
        return false;
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Color(0xFFF42408),
        body: SafeArea(
          child:loading?Loading(): Center(
            child: Stack(
              children: <Widget>[

               broadcasterLeft? Center(
                 child: Container(
                   child: Center(
                     child: Text(
                       "Live ended by broadcaster",
                       style: TextStyle(color: Colors.white),
                     ),
                   ),
                 ),
               ): _viewRows(),
                Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Colors.transparent, Color(0xA8000000)],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          stops: [.5, 1])),
                  child: Column(
                    children: [
                      //close of broadcasting
                      Container(
                        alignment: Alignment.topRight,
                        child: Container(
                          child: IconButton(
                            onPressed: (){

                              if(widget.broadcaster){
                                int seconds = timerServices.getCurrentTime();
                                closeLiveModal(context,widget.channelName,seconds);
                                //timerServices.close();
                              }else{
                                Navigator.of(context).pop();
                              }

                            },
                            icon: Icon(Icons.cancel,color: Color(0x88000000),),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          child: _panel(),
                        ),
                      ),
                    Container(
                        height: MediaQuery.of(context).size.height * 0.1,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.transparent,
                        child:   widget.broadcaster? Row(
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  IconButton(
                                      onPressed: () {
                                        openModalBottomSheetForMessage(context,widget.channelName,_comment);
                                      },
                                      icon: Icon(
                                        Icons.message_outlined,
                                        color: Colors.white,
                                      )),
                                  IconButton(
                                      onPressed: () {},
                                      icon: Icon(
                                        Icons.menu,
                                        color: Colors.white,
                                      )),
                                  IconButton(
                                      onPressed: () {
                                        showModalBottomSheet(
                                            context: context,
                                            isScrollControlled: true,
                                            isDismissible: false,
                                            backgroundColor: Colors.transparent,
                                            builder: (context) {
                                              return StatefulBuilder(
                                                  builder: (context, state) {
                                                    return DraggableScrollableSheet(
                                                      expand: false,
                                                      initialChildSize: .6,
                                                      minChildSize: .6,
                                                      maxChildSize: .6,
                                                      builder: (context,
                                                          scrollController) {
                                                        return Container(
                                                            decoration: BoxDecoration(
                                                                color: Colors.white,
                                                                borderRadius:
                                                                BorderRadius.only(
                                                                    topLeft: Radius
                                                                        .circular(
                                                                        30),
                                                                    topRight: Radius
                                                                        .circular(
                                                                        30))),
                                                            child:
                                                            DefaultTabController(
                                                              length: 2,
                                                              child: Column(
                                                                children: [
                                                                  Container(
                                                                    padding: EdgeInsets.only(left: 50),
                                                                    height: MediaQuery.of(context).size.height *.07,
                                                                    child: Row(
                                                                      children: [
                                                                        Expanded(
                                                                            child:TabBar(
                                                                              labelStyle: TextStyle( color: Colors.red, fontWeight:FontWeight.w600,fontSize:17),
                                                                              unselectedLabelStyle: TextStyle(color: Colors.black,fontWeight:FontWeight.w600,fontSize:17),
                                                                              indicatorColor:Colors.red,
                                                                              labelColor:Colors.red,
                                                                              unselectedLabelColor:Colors.black,
                                                                              tabs: [
                                                                                Container(
                                                                                  child: Text("WAITING"),
                                                                                ),
                                                                                Container(
                                                                                  child: Text("GUEST LIVE"),
                                                                                )
                                                                              ],
                                                                            )),
                                                                        Container(
                                                                          width: 60,
                                                                          child: IconButton(
                                                                            onPressed: (){
                                                                              Navigator.of(context).pop();
                                                                            },
                                                                            icon:Icon( Icons.clear),
                                                                          ),
                                                                        )
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  Expanded( child:Container(
                                                                    padding: EdgeInsets.only(left: 20),
                                                                    child: TabBarView(
                                                                      children: [
                                                                        VideoLiveWaitingList(channelName:widget.channelName,broadcaster: widget.broadcaster,),
                                                                        VideoLiveGuestList(channelName:widget.channelName,broadcaster: widget.broadcaster,)
                                                                      ],
                                                                    ),
                                                                  )),
                                                                  // widget.broadcaster
                                                                  //     ? Container()
                                                                  //     : AudioVideoJoin(channelName: widget.channelName,onStartListen:onStartListen ,)
                                                                ],
                                                              ),
                                                            ));
                                                      },
                                                    );
                                                  });
                                            });
                                      },
                                      icon: Icon(
                                        Icons.people_alt_outlined,
                                        color: Colors.white,
                                      )),
                                ],
                              ),
                            )
                          ],
                        ):
                      Container(
                        height: MediaQuery.of(context).size.height*0.1,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.transparent,
                        child: Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 15),
                              width: MediaQuery.of(context).size.width * 0.6,
                              child: TextFormField(
                                controller: _comment,

                                decoration: InputDecoration(
                                  suffixIcon:  IconButton(onPressed: (){
                                    if(_comment.text.isNotEmpty){
                                      FirestoreServices.insertNewComment(widget.channelName, _comment.text);
                                      _comment.text="";
                                    }

                                  }, icon:   Icon(
                                    Icons.send,
                                    color: Colors.red,
                                  ),),
                                    fillColor: Colors.grey[300],
                                    filled: true,
                                    isDense: true,
                                    hintText: "Comments",
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(90.0)),
                                        borderSide: BorderSide.none)),
                              ),
                            ),

                      Container(
                        width: MediaQuery.of(context).size.width * 0.4,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                              ButtonTheme(
                                minWidth:60,
                                child: RaisedButton(

                                    onPressed:()async{
                                      await FirestoreServices.joinVideo(1, widget.channelName);
                                    },

                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30),),
                                    color:Color(0xFFF42408),
                                child:Container(
                                  child: Text("Join",style: TextStyle(fontSize: 15,color: Colors.white,fontWeight: FontWeight.w500),),
                                )
                                ),
                              )
                            // Icon(
                            //   Icons.share_outlined,
                            //   color: Colors.black,
                            // ),
                            // Icon(
                            //   Icons.flip_camera_ios_outlined,
                            //   color: Colors.black,
                            // ),
                            // Icon(
                            //   Icons.mic_none_outlined,
                            //   color: Colors.black,
                            // ),
                          ],
                        ),
                      )
                          ],
                        ),
                      )
                      ,
                      )
                      // Container(
                      //   height: MediaQuery.of(context).size.height*0.1,
                      //   width: MediaQuery.of(context).size.width,
                      //   color: Colors.white,
                      //   child: Row(
                      //     children: [
                      //       Container(
                      //         padding: EdgeInsets.only(left: 15),
                      //         width: MediaQuery.of(context).size.width * 0.6,
                      //         child: TextFormField(
                      //           controller: _comment,
                      //           decoration: InputDecoration(
                      //               fillColor: Colors.grey[300],
                      //               filled: true,
                      //               isDense: true,
                      //               hintText: "Comments",
                      //               border: OutlineInputBorder(
                      //                   borderRadius: BorderRadius.all(
                      //                       Radius.circular(90.0)),
                      //                   borderSide: BorderSide.none)),
                      //         ),
                      //       ),

                      // Container(
                      //   width: MediaQuery.of(context).size.width * 0.4,
                      //   child: Row(
                      //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      //     children: [
                      //     IconButton(onPressed: (){
                      //       if(_comment.text.isNotEmpty){
                      //         FirestoreServices.insertNewComment(widget.channelName, _comment.text);
                      //         _comment.text="";
                      //       }
                      //
                      //     }, icon:   Icon(
                      //       Icons.send,
                      //       color: Colors.red,
                      //     ),),
                      //       Icon(
                      //         Icons.share_outlined,
                      //         color: Colors.black,
                      //       ),
                      //       Icon(
                      //         Icons.flip_camera_ios_outlined,
                      //         color: Colors.black,
                      //       ),
                      //       Icon(
                      //         Icons.mic_none_outlined,
                      //         color: Colors.black,
                      //       ),
                      //     ],
                      //   ),
                      // )
                      //     ],
                      //   ),
                      // )
                    ],
                  ),
                ),
                //    _panel(),
                // _toolbar(),

                LiveHeader(
                    broadcastUser: widget.broadcastUser,
                    broadcaster: widget.broadcaster,
                    channelName: widget.channelName,
                    onClose:(){
                      if(widget.broadcaster){
                        int seconds = timerServices.getCurrentTime();
                        closeLiveModal(context,widget.channelName,seconds);
                        timerServices.close();
                      }else{
                        Navigator.of(context).pop();
                      }
                    },
                    onView:(){
                      openAllViewerModal(context,widget.channelName);
                    }
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class AudioVideoJoin extends StatefulWidget {
  final String channelName;
  final Function onStartListen;
  AudioVideoJoin({this.channelName,this.onStartListen});
  @override
  _AudioVideoJoinState createState() => _AudioVideoJoinState();
}

class _AudioVideoJoinState extends State<AudioVideoJoin> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * .1,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          RaisedButton(
            onPressed: () async{
              await FirestoreServices.joinVideo(1, widget.channelName);
              widget.onStartListen();
            },
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
                side: BorderSide(color: Colors.red)),
            color: Colors.white,
            child: Row(
              children: [
                Icon(
                  Icons.mic_none_outlined,
                  color: Colors.black,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "Audio Join",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                )
              ],
            ),
          ),
          RaisedButton(
            onPressed: () async{
              await  FirestoreServices.joinVideo(2, widget.channelName);
              widget.onStartListen();
            },
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            color: Colors.red,
            child: Row(
              children: [
                Icon(
                  Icons.videocam,
                  color: Colors.white,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "Video Join",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.white),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class VideoLiveWaitingList extends StatefulWidget {
  final String channelName;
  final bool broadcaster;
  VideoLiveWaitingList({this.channelName, this.broadcaster});

  @override
  _VideoLiveWaitingListState createState() => _VideoLiveWaitingListState();
}

class _VideoLiveWaitingListState extends State<VideoLiveWaitingList> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: StreamBuilder(
          stream: FirestoreServices().getVideoWaitingViewer(widget.channelName),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Container();
            }
            List<ViewerModel> viewers = snapshot.data;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: Text(
                    "Waiting List (${snapshot.data.length})",
                    style: TextStyle(color: Colors.grey[600]),
                  ),
                ),
                Container(
                  child: ListView.separated(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: viewers.length,
                    itemBuilder: (context, i) {
                      ViewerModel viewer = viewers[i];
                      print(viewer);
                      return ListTile(
                        leading: viewer.photoUrl !=null ? CircleAvatar(
                          radius: 20,
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(40),
                              child: HexagonProfilePicNetworkImage(
                                url: viewer.photoUrl,
                              )),
                        )
                            : InitIconContainer(
                          radius: 40,
                          text: viewer.name,
                        ),
                        title:Text(viewer.name,style: TextStyle(fontSize: 14),),

                        trailing: widget.broadcaster?  Container(
                          width: 90,
                          height: 25,
                          child: RaisedButton(

                            onPressed: () {

                              FirestoreServices.addUserToVideoLive(viewer, widget.channelName);
                              // CallServices.joinVideo(2, widget.channelName);
                            },
                            shape:
                            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                            color: Colors.red,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(
                                  viewer.status==1? Icons.mic_none_outlined:Icons.videocam,
                                  color: Colors.white,
                                  size: 15,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  "Accept",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12,
                                      color: Colors.white),
                                )
                              ],
                            ),
                          ),
                        ):Container( width: 1,),
                      );
                    },
                    separatorBuilder: (context,i){
                      return Divider(thickness: 1,);
                    },
                  ),)
              ],
            );
          },
        ));
  }
}

class VideoLiveGuestList extends StatefulWidget {

  final String channelName;
  final bool broadcaster;
  VideoLiveGuestList({this.channelName, this.broadcaster});

  @override
  _VideoLiveGuestListState createState() => _VideoLiveGuestListState();
}

class _VideoLiveGuestListState extends State<VideoLiveGuestList> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: StreamBuilder(
          stream: FirestoreServices().getVideoGuestViewer(widget.channelName),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Container();
            }
            List<ViewerModel> viewers = snapshot.data;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: Text(
                    "Guest List (${snapshot.data.length})",
                    style: TextStyle(color: Colors.grey[600]),
                  ),
                ),
                Container(
                    child: ListView.separated(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: viewers.length,
                      itemBuilder: (context, i) {
                        ViewerModel viewer = viewers[i];
                        print(viewer);
                        return ListTile(
                            leading:viewer.photoUrl !=null ? CircleAvatar(
                              radius: 20,
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(40),
                                  child: HexagonProfilePicNetworkImage(
                                    url: viewer.photoUrl,
                                  )),
                            )
                                : InitIconContainer(
                              radius: 40,
                              text: viewer.name,
                            ),
                            title: Text(viewer.name,style: TextStyle(fontSize: 14),)
                        );
                      },
                      separatorBuilder: (context,i){
                        return Divider(thickness: 1,);
                      },
                    ))
              ],
            );
          },
        ));

  }
}
