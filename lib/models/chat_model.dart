class ChatModel{
  final String userId;
  final String name;
  final DateTime time;
  final String message;
  final bool defaultMessage;
  ChatModel({this.userId,this.name,this.time,this.message,this.defaultMessage = false});
}