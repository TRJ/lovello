class LiveModel{
  final String liveId;
  final String name;
  final String userId;
  final String photoUrl;
  final bool live;
  final DateTime startTime;
  final String liveName;
  final String liveType;
  final int seats;

  LiveModel({this.live,this.name,this.userId,this.liveId,this.startTime,this.liveName,this.photoUrl,this.liveType,this.seats});
}