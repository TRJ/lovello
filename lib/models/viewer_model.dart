class ViewerModel{
  final String uid;
  final String name;
  final String countryFlag;
  final String photoUrl;
  final int gender;
  final int age;
  final int status;
  ViewerModel({this.uid,this.name,this.countryFlag,this.photoUrl,this.gender,this.age,this.status});
}