class CurrentUserModel{
 final String userId;
 final String uniqueId;
 final String nickName;
 final String name;
 final String phone;
 final String image;
 final String email;
 final String runningLevel;
 final String broadcastingLevel;
 final String badge;
 final String badgecategory;
 final String loginStatus;
 final String deviceId;
 final String talent;
 final String countryId;
 final String countryName;
 final String token;
 CurrentUserModel(
     {this.userId,
       this.uniqueId,
       this.nickName,
       this.name,
       this.phone,
       this.image,
       this.email,
       this.runningLevel,
       this.broadcastingLevel,
       this.badge,
       this.badgecategory,
       this.loginStatus,
       this.deviceId,
       this.talent,
       this.countryId,
       this.countryName,
       this.token});

}