import 'dart:io';

import 'package:hive/models/country_model.dart';

class SignupDataModel{
  final String name;
  final String email;
  final String password;
   String otp;
  File image;
  CountryModel country;
  String dob;
  SignupDataModel({this.name,this.email,this.password,this.otp,this.image,this.country,this.dob});
}