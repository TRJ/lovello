import 'package:flutter/material.dart';
import 'package:hive/landing/splash.dart';
import 'package:hive/screens/verifications/set_gender_and_country.dart';
import 'package:hive/screens/verifications/sign_in.dart';
import 'package:hive/screens/verifications/sign_up.dart';
import 'package:hive/screens/verifications/signup_with_otp.dart';
import 'package:hive/screens/verifications/terms_condition.dart';
import 'package:hive/screens/withAuth/go_live/go_live_landing.dart';
import 'package:hive/screens/withAuth/landing/landing_page.dart';


class RouteGenerator{
  static Route<dynamic> generateRoute(RouteSettings settings){
    switch (settings.name){
      case '/':
        return MaterialPageRoute(builder: (_)=>SpashScreen());
      case '/login':
        return MaterialPageRoute(builder: (_)=>NewSignInDesign());
      case '/sign_up':
        return MaterialPageRoute(builder: (_)=>NewSignupDesign());
      case '/signup_otp':
        var arg = settings.arguments;
        return MaterialPageRoute(builder: (_)=>SingupWithOTP(data: arg,));
      case '/terms_condition':
        var arg = settings.arguments;
        return MaterialPageRoute(builder: (_)=>TermsCondition(data: arg,));
      case '/set_gender_country':
        var arg = settings.arguments;
        return MaterialPageRoute(builder: (_)=>SetGenderAndCountry(data: arg,));
      case '/landing':
        return MaterialPageRoute(builder: (_)=>LandingPage());




      case '/live_landing':
        return MaterialPageRoute(builder: (_)=>GoLiveLanding());
    }
  }
}