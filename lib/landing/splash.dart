
import 'package:flutter/material.dart';
import 'package:hive/services/shared_storage_services.dart';

class SpashScreen extends StatefulWidget {
  @override
  _SpashScreenState createState() => _SpashScreenState();
}

class _SpashScreenState extends State<SpashScreen> {
  @override
  void initState() {
    new Future.delayed(const Duration(seconds: 2), () async{
      Navigator.popAndPushNamed(context, '/login');
       var token = await LocalStorageHelper.read("token");
      if(token==null){
        Navigator.popAndPushNamed(context, '/login');
      }else{
        Navigator.popAndPushNamed(context, '/landing');
      }

    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: SafeArea(
        child: Container(
            child: Center(
              child: Image(
                image: AssetImage("images/lowello_logo.png"),
              ),
            ),
        ),
      ),
    );
  }
}
