

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hive/models/chat_model.dart';
import 'package:hive/models/current_user_model.dart';
import 'package:hive/models/live_model.dart';
import 'package:hive/models/viewer_model.dart';
import 'package:hive/services/token_manager_services.dart';

class FirestoreServices {
  // static Future setLocalDescription(String description) async {}
  //
  static Future onWillPop(String id) async {
    print(id);
    try {
      var _fire = FirebaseFirestore.instance;
      await _fire.collection("live_streams").doc(id).update({"live": false});
    } catch (e) {}
  }

  static Future onGuestLeave(String id) async {
    try {
      CurrentUserModel user = await TokenManagerServices().getData();
      await FirebaseFirestore.instance.collection("live_streams").doc(id)
          .collection("viewers").doc(user.userId)
          .update({
        "status": 5
      });
    } catch (e) {}
  }

  static Future createNewLive(String id, String liveName, BuildContext context,
      int type, int seats) async {
    try {

      var _fire = FirebaseFirestore.instance;
      CurrentUserModel user = await TokenManagerServices().getData();

      await _fire.collection("live_streams").doc(id).set({
        "id": id,
        "uid": user.userId,
        "name": user.name,
        "photoUrl": user.image,
        "live": true,
        "live_name": liveName != "" ? liveName : user.name,
        "start_time": DateTime.now(),
        'seats': seats,
        "type": type == 1 ? "video" : type == 0 ? "multi_video" : "audio"
        //Tobe changed later
      });
      await _fire.collection("live_streams").doc(id).collection("chats").add({
        "user_id": user.userId,
        "name": user.name,
        "message": "Started",
        "time": DateTime.now()
      });
    } catch (e) {
      print(e);
    }
  }

  static Future newUserJoined(String channelName)async{
    CurrentUserModel user = await TokenManagerServices().getData();
    await FirebaseFirestore.instance.collection("live_streams").doc(channelName).collection("viewers").doc(user.userId).get().then((value){
      if(!value.exists){
        value.reference.set({
          "uid":user.userId,
          "name":user.name,
          "photoUrl":user.image,
          "status":0, // 0=>viwer; 1=>requesting audio; 2=> requesting video; 3=> live audio; 4=> live video; 5=> left
          "accepted_time":DateTime.now()
        });
      }
    });
  }
//
// /*
//   * Insert New Comment
//   * */
//
  static Future insertNewComment(String id, String message) async {
    try {

      var _fire = FirebaseFirestore.instance;
      CurrentUserModel user = await TokenManagerServices().getData();
      await _fire.collection("live_streams").doc(id).collection("chats").add({
        "user_id": user.userId,
        "name": user.name,
        "message": message,
        "time": DateTime.now()
      });
    } catch (e) {}
  }

  Stream<List<ChatModel>> getChats(id) {
    final chats = FirebaseFirestore.instance
        .collection("live_streams")
        .doc(id)
        .collection("chats")
        .orderBy('time', descending: true)
        .snapshots()
        .map(_getChatsFromSnapshot);

    return chats;
  }

  List<ChatModel> _getChatsFromSnapshot(QuerySnapshot snap) {
    if (snap.docs.length == 0) return [];
    return snap.docs.map((e) {
      Map<String, dynamic> data = e.data() as Map<String, dynamic>;
      return ChatModel(
          userId: data['user_id'],
          name: data['name'],
          time: DateTime.fromMillisecondsSinceEpoch(
              data['time'].seconds * 1000),
          message: data['message']);
    }).toList();
  }

  static Future joinVideo(int type, String channelName)async{// type == 1=>requesting audio; type == 2=> requesting video;

    CurrentUserModel user = await TokenManagerServices().getData();
    await FirebaseFirestore.instance.collection("live_streams").doc(channelName).collection("viewers").doc(user.userId).update({
      "status":type,
    });

  }

  Stream<List<ViewerModel>> getViewersList(String channelName){
    return FirebaseFirestore.instance.collection("live_streams").doc(channelName).collection("viewers").orderBy("accepted_time",descending: true).limit(4).snapshots().map(_getVideoWaitingViewerFromSnapshot);
  }


  Stream<List<ViewerModel>> getVideoViewers(String channelName){
    return FirebaseFirestore.instance.collection("live_streams").doc(channelName).collection("viewers").snapshots().map(_getVideoWaitingViewerFromSnapshot);
  }



  Stream<List<ViewerModel>> getVideoWaitingViewer(String channelName){
    return FirebaseFirestore.instance.collection("live_streams").doc(channelName).collection("viewers").where('status',whereIn: [1,2]).snapshots().map(_getVideoWaitingViewerFromSnapshot);
  }

  List<ViewerModel> _getVideoWaitingViewerFromSnapshot(QuerySnapshot q){
    print(q.docs.length);
    if (q.docs.length == 0) {
      return [];
    }


    return q.docs.map((DocumentSnapshot doc){
      Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
     return ViewerModel(
          uid: doc.id,
          name: data['name'],
          photoUrl: data['photoUrl'],
          status: data['status']
      );
    }
    ).toList();
  }

  Stream<List<ViewerModel>> getVideoGuestViewer(String channelName){
    return FirebaseFirestore.instance.collection("live_streams").doc(channelName).collection("viewers").where('status',whereIn: [3,4]).orderBy("accepted_time",descending: false).snapshots().map(_getVideoWaitingViewerFromSnapshot);
  }


  static Future addUserToVideoLive(ViewerModel viewer,String channelName)async{
    await FirebaseFirestore.instance.collection("live_streams").doc(channelName).collection("viewers").doc(viewer.uid).update({
      "status":viewer.status==1?3:4,
      "accepted_time":DateTime.now()
    });
  }


  Stream<List<LiveModel>> getLiveStreams(type) {
    if(type=="both"){
      final lives = FirebaseFirestore.instance
          .collection("live_streams")
          .where("live", isEqualTo: true)
          .where("type", whereIn: ["video","multi_video"])
          .snapshots()
          .map(_getLiveStreamsFromSnapshot);
      return lives;
    }else{
      final lives = FirebaseFirestore.instance
          .collection("live_streams")
          .where("live", isEqualTo: true)
          .where("type", isEqualTo: type)
          .snapshots()
          .map(_getLiveStreamsFromSnapshot);
      return lives;
    }
  }

  List<LiveModel> _getLiveStreamsFromSnapshot(QuerySnapshot snapshot) {
    if (snapshot.docs.length == 0) {
      return [];
    }

    return snapshot.docs.map((e) {
      Map<String, dynamic> data = e.data() as Map<String, dynamic>;
      return LiveModel(
        live:data['live'],
        liveId: e.id,
        userId: data['uid'],
        name: data['name'],
        photoUrl: data['photoUrl'],
        liveName: data['live_name'],
        liveType: data['type'],
        seats:data['seats'],
        startTime: DateTime.fromMillisecondsSinceEpoch(
            data['start_time'].seconds * 1000),
      );
    }).toList();
  }
//
//   static Future<String> uploadImageToFirebase(_imageFile, filePath) async {
//     final FirebaseAuth auth = FirebaseAuth.instance;
//     final User user = auth.currentUser;
//
//     String newFileName = user.uid + filePath;
//     Reference firebaseStorageRef =
//     FirebaseStorage.instance.ref().child('$filePath/$newFileName');
//     UploadTask uploadTask = firebaseStorageRef.putFile(_imageFile);
//     TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() => null);
//     return taskSnapshot.ref.getDownloadURL().then((value) {
//       return value;
//     });
//   }
//
//   static Future<String> uploadCoverPic(_imageFile, filePath, index) async {
//     final FirebaseAuth auth = FirebaseAuth.instance;
//     final User user = auth.currentUser;
//
//     String newFileName = user.uid + filePath + index;
//     Reference firebaseStorageRef =
//     FirebaseStorage.instance.ref().child('$filePath/$newFileName');
//     UploadTask uploadTask = firebaseStorageRef.putFile(_imageFile);
//     TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() => null);
//     return taskSnapshot.ref.getDownloadURL().then((value) {
//       return value;
//     });
//   }
//
//   static Future editProfileInfo(File profilePic,
//       List<File> coverPics,
//       List<String> coverPicUrls,
//       String name,
//       String location,
//       int gender,
//       List<String> tags,
//       String introduction,
//       String constellation) async {
//     try {
//       final FirebaseAuth auth = FirebaseAuth.instance;
//       final User user = auth.currentUser;
//       await user.updateProfile(displayName: name);
//       if (profilePic != null) {
//         String profilePicUrl =
//         await uploadImageToFirebase(profilePic, 'profile_pic');
//         print(profilePicUrl);
//         await user.updateProfile(photoURL: profilePicUrl);
//       }
//
//       for (var i = 0; i < coverPics.length; i++) {
//         if (coverPics[i] != null) {
//           String coverUrl =
//           await uploadCoverPic(coverPics[i], "cover_pic", i.toString());
//           coverPicUrls[i] = coverUrl;
//         }
//       }
//
//       Map<String, dynamic> data = {
//         'location': location.isEmpty ? null : location,
//         'gender': gender,
//         'tags': tags,
//         'introduction': introduction.isEmpty ? null : introduction,
//         'contellation': constellation.isEmpty ? null : constellation,
//         'cover_pics': coverPicUrls
//       };
//
//       var update = await FirebaseFirestore.instance
//           .collection("users")
//           .doc(user.uid)
//           .update(data);
//       return 1;
//     } catch (e) {
//       print(e);
//     }
//   }
//
//   static Future createNewUser(User user, String name) async {
//     await FirebaseFirestore.instance
//         .collection("lovello_ids")
//         .doc("max")
//         .get()
//         .then((d) async {
//       var max = d.data()['max'] ?? 0;
//       max += 1;
//
//       var update = await FirebaseFirestore.instance
//           .collection("users")
//           .doc(user.uid)
//           .set({
//         'location': null,
//         'name': name,
//         'photoUrl': user.photoURL,
//         "gender": 2,
//         "birthday": null,
//         'tags': [],
//         "introduction": null,
//         "contellation": null,
//         "cover_pics": [null, null, null, null, null],
//         'fans': 0,
//         'following': 0,
//         'level': 0,
//         'beans': 0,
//         'diamond': 0,
//         'bio': null,
//         "countryId": null,
//         "countryName": null,
//         "countryFlag": null,
//         "preference_ids": [],
//         "preference_names": [],
//         'lovello_id': max.toString(),
//         'lovello_id_edited': false
//       });
//
//       d.reference.update({"max": max});
//     });
//   }
//
//   Stream<List<PersonalChatModel>> getChat(String channelId) {
//     print(channelId);
//     return FirebaseFirestore.instance
//         .collection("chat")
//         .doc(channelId)
//         .collection("messages")
//         .orderBy('createdAt')
//         .snapshots()
//         .map(_getPersonalChatsMessagesFromSnapshot);
//   }
//
//   List<PersonalChatModel> _getPersonalChatsMessagesFromSnapshot(
//       QuerySnapshot snapshot) {
//     print(snapshot.docs.length);
//     return snapshot.docs.map((d) {
//       Timestamp createdAt = d.data()['createdAt'];
//       print(createdAt);
//       return PersonalChatModel(
//           id: d.data()['id'],
//           text: d.data()['text'],
//           createdAt:
//           DateTime.fromMillisecondsSinceEpoch(createdAt.seconds * 1000));
//     }).toList();
//   }
//
//   static Future storeChat(String message, String id, String channelName) async {
//     try {
//       await FirebaseFirestore.instance
//           .collection("chat")
//           .doc(channelName)
//           .collection("messages")
//           .add({
//         'text': message,
//         'id': id,
//         'createdAt': DateTime.now(),
//       });
//     } catch (e) {
//       print(e);
//     }
//   }
//
  static Future storeMultiGuests(String channelName, String docId) async {
    try {
      CurrentUserModel user = await TokenManagerServices().getData();
      await FirebaseFirestore.instance.collection("live_streams").doc(
          channelName).collection("multi_guests").doc(docId).set({
        "id": user.userId,
        "name": user.name,
        "photoUrl": user.image
      });
    } catch (e) {
      print(e);
    }
  }

  static Future<Map<int,dynamic>> getMultiGuests(String channelName) async {
    try{
      Map<int,dynamic> guests = await FirebaseFirestore.instance.collection("live_streams").doc(
          channelName).collection("multi_guests").get().then((q) {
          Map<int,dynamic> g = {};
          q.docs.forEach((e) {
            g[int.parse(e.id)] = {
              "id":e.data()["id"],
              "name":e.data()["name"],
              "photoUrl":e.data()["photoUrl"]
            };
          });

          return g;
      });
      return guests;
    }catch(e){
      print(e);
    }
  }
}

