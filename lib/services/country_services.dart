import 'dart:convert';

import 'package:hive/models/country_model.dart';
import 'package:hive/screens/shared/urls.dart';

import 'package:http/http.dart' as http;
class CountryServices {
  static Future<List<CountryModel>> getAllCountries() async{
    List<CountryModel> list = [];
    try{
        var url =Uri.parse(countryList);
        var response = await http.get(url);
        if(response.statusCode==200){
          var info = jsonDecode(response.body);
          var success = info['success'];
          if(success){
            var data = info['data'];
            if(data.length>0){
              for(var i=0;i<data.length;i++){
                if(data[i]['status'] == "Active"){
                  list.add(CountryModel(
                      id: data[i]["id"],
                      region: data[i]['region'],
                      name: data[i]['name'],
                      iso: data[i]['iso'],
                      isoCode: data[i]['isd_code'],
                      flag: data[i]['flag_img'],
                      status: data[i]['status']
                  ));
                }


              }
            }
          }
        }

    }catch(e){
      print(e);
    }
    return list;
  }
}
