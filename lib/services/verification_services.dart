import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:hive/models/device_info_model.dart';
import 'package:hive/models/signup_data_model.dart';
import 'package:hive/screens/shared/urls.dart';
import 'package:hive/services/helper_services.dart';
import 'package:hive/services/token_manager_services.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as path;
class VerificationServices{



 Future<int> doLogin(String type,String email,String password)async{
    try{
      DeviceInfoModel deviceInfo = await HelperServices.getDeviceInfo();
      LocationData locationData = await HelperServices.getCurrentLocation();
      if(type=="email"){
        Map payload = {
          "type":"email",
          "email": email,
          "device_id": deviceInfo.identifier,
          "password": password,
          "longitude": locationData.longitude,
          "latitude": locationData.latitude
        };
        var body = jsonEncode(payload);
        print(body);
        var url = Uri.parse(loginUrl);
        var response = await http.post(url,body: body,headers: {"Content-Type": "application/json"});
        print(response.statusCode);
        print(response.body);
        if(response.statusCode==200){
        try{

            var info = jsonDecode(response.body);
            if(info['success']){
              var data = info['data'];
              if(data !=null){
                await TokenManagerServices().saveData(data);
                return 1;
              }else throw Error();
            }else throw Error();

        }catch(e){
          print(e);
          return 0;
        }
      }else throw Error();

      }
    }catch(e){
      print(e);
    }
  }



 Future<int> doSignUp({String type,String email})async{
   try{
    String body;

    if(type=='email'){
      Map payload = {
        "type": type,
        "email": email
      };

      body = jsonEncode(payload);
    }

    var url = Uri.parse(signupOtp);
    var response = await http.post(url,body: body,headers: {"Content-Type": "application/json"});
    if(response.statusCode==200){
      try{
        var info = jsonDecode(response.body);
        if(info['success']){
          print(info['message']);
          return 1;
        }else throw Error();
      }catch(e){
        return 2;
      }
    }else throw Error();

   }catch(e){
     print(e);
   }
  }

  Future<List> verifySignUp(SignupDataModel data)async{
    try{

      DeviceInfoModel deviceInfo = await HelperServices.getDeviceInfo();
      LocationData locationData = await HelperServices.getCurrentLocation();
      print(data.country.isoCode);
      FormData formData = FormData.fromMap({
          'email':data.email,
          'country_code':data.country !=null?data.country.isoCode:null,
          'name':data.name,
          'nick_name':data.name,
          'device_id':deviceInfo.identifier,
          'password':data.password,
          'country_id':data.country !=null?data.country.id:null,
          'otp':data.otp,
          'longitude':locationData.longitude,
          'latitude':locationData.latitude,
          'dob':data.dob,
          'profile_img':data.image !=null? await MultipartFile.fromFile(data.image.path,filename:path.basename(data.image.path)):null
      });

      Dio dio = Dio();
      Response response = await dio.post(
        verifySignup,
        data: formData,
        options: Options(contentType: 'multipart/form-data'),
      );

      if(response.statusCode==200){
        var info = response.data;
        print(info);
        try{

          if(info['success']){
              var data = info['data'];
              await TokenManagerServices().saveData(data);
              return [1];
          }else throw Error();
        }catch(e){
          return [2,info['message']];
        }

      }


    }catch(e){
      print(e);
    }
  }



}